# TODO list
**Written by**: Feng Lu

**Affiliation**: National Astronomical Observatories, Chinese Academy of Sciences

**Contacts**: <Jacobfeng@gmail.com>

####2014. Jul. 2:

1. changed the folder structure. changed the default example_cfg.cfg's location in the code, need to be tested. (**DONE**)
2. need to **implement PBS script**. (** DONE **)

#### 2014. Jul. 7
1. Need following feature
	1. laser central frequency needs to be able to be shifted, arbitrary value or random value with given distribution.
	2. simulation need to be able to be stopped in the middle of pulse, current calculation is through the whole give pulse. it needs to be changed. [implemented for circular polarization code, haven't tested yet)
	3. ConfigObj doesn's support list of pairs. still need to attributes to store each individual value of the pair. probably with fseek().
	4. A GUI? wx/pygtk/qt are not supported on the server, either install one or find some alternative.
2. Test to be done
	1. photon return with different frequency offset & intensity
	2. photon return with different excitation duration & intensity
Well this is a lot of works!
3. One thing needs to figure out is how Edward calculate the beam proflile in his report.
