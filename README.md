# Simulation Code for Sodium Laser Guide Star Photon Return Simulation

**Written by**: Feng Lu

**Affiliation**: National Astronomical Observatories, Chinese Academy of Sciences

**Contacts**: <Jacobfeng@gmail.com>

**Version**: 1.0


## Updates 
 *Version* | *Update Contents* 
 --------- | ------------------
 0.91      | Add README.md file, still much need to be added 
 1.0       | Linear/Ciruclar polarization code finished and tested, README elaborated.

## License
This software is under GNU GPL version 3.0 license. For detailed description of the 
license, please resort to the LICENSE file under the root folder.


## Introduction
### Installation
#### Prerequisites
This software relies on quite a few open source libraries. These libraries need to be installed prior to the compiling of this software.

For *C/C++*, following libraries are needed (library name, link, version by the time this software is written):

1. *libconfig*, <http://www.hyperrealm.com/libconfig/>, 1.4.9
2. *GNU GSL*, <http://mirror.hust.edu.cn/gnu/gsl/>, 1.16

For *python*, following libraries are needed:

1. Jinja2, <https://github.com/mitsuhiko/jinja2>, 2
2. Pandas, <http://pandas.pydata.org>, 0.14.0
3. configobj, <https://pypi.python.org/pypi/configobj/5.0.5>, 5.0.5
4. python distribution with Scipy/Numpy/Matplotlib included like Enthougt Canopy or Anaconda, <https://www.enthought.com>, Canopy 1.3

For generating and importing data file, Ipython Notebook is used, codes in the notebook could be easily turned into python script. One could use any of these two methods. For running Ipython notebook, ipython needs to be installed along with canopy or anaconda.

#### Make
The software is quite a simple piece, which doesn't need a complicated Makefile to compile and link it. In the ./src folder, a Makefile is provided, with 

``
$ make cp
``

to make a circular polarization simulation executable under ./bin folder, or with 

``
$ make lp
``

to make a linear polarization simulation executable.

### Running the simulation
#### Input Parameter File Generation
This software runs simulation based on input parameter file. The executable syntax of the program is simple:

``
$ <cp|lp> [parameter_filename]
``

An examplary parameter file cfg_example.cfg is put under the ./example folder, and this file will also be used when parameter_filename argument is not provided. The parameter file's format strictly follows the parsing format rule of libconfig library, which has section enclosed with `[]`, keys/values paris like `key=value`.

A much easier way to generate these cfg file is through the `CFG generator.ipynb`, which is self-explanatory how to use it.

#### Parameter Description 
With the ``cfgGenerator.py``, all the dictated parameters will be written into relevant CFG configuration file. If you are interested in the structure of the CFG file, an example is provided ``data\example\cfg_example.cfg`` with description of each parameter. 

Since it is more convenient to use cfgGenerator.py to generate large amount of CFG files automatically, we will focus on how to specify parameter in this script rather than manually write CFG file individually. 

After open the ``cfgGenerator.py``, you will see a section of the following:

```
version = "1";
name = "circular polarization Lijiang sometime in Lijiang";
mass_sodium = 38.18E-27;
mass_nitrogen = 46.48E-27;
temperature = 185.;
thetas = 72E-20;

K = 1.3806488E-23;
nu_na = 5.09E14;
ns = 6.704e19;
freq_fwhm_doppler_mhz = 1000.;

magnetic_field_related_list = [{'time_larmor_precession_s': 3.1072E-6, 
                                'radian_emf_lb': 2.2852}];
I_sat_list = [65.4];

intensity_list = logspace(log10(0.1), log10(10), num=2, base=10);
repetition_frequency_list = [500.];

pulse_format_file_list = [{'intensity_filename' : '../data/const/measured_pulse/filtered_data_int_1.bin', 
                           'time_filename' : '../data/const/measured_pulse/filtered_data_time_1.bin', 
                           'intensity_file_datalength': 5333}];

spectrum_format_file_list = [{'spectrum_filename' : '../data/const/spectrum/spectrum_power.bin',
                              'frequency_filename' : '../data/const/spectrum/spectrum_freq.bin',
                              'idx_freq_length' : 960}];

spectrum_simulation_parameter_list = [{'freq_nu_start_mhz' : -1400.,
                                      'freq_nu_end_mhz' : 1400.,
                                      'freq_nu_incr_mhz' : 5.}];

EOM_list = [{'eom_flag' : 1,
             'percentage' : 0.12}
            ];

chirp_list = [{'flag_chirp' : 0,
               'chirp_rate_mhz' : 0.}];

random_generator_list = [{'flag_use_random_seed' : 1,
                          'seed_int' : 0,
                          'type' : 'gsl_rng_mt19937'}];

start_state_list = [{'flag_start_M_specific' : 0,
                     'start_M_level' : 7}];

trials_list = [100];
time_delta_s = 30E-9;
step_per_mhz = 20;

print_each_step = 0;
print_photon_return = 1;
print_matrix = 0;
print_sm = 0;

simulation_flag_list = [{'flag_spin_exchange' : 1,
'flag_oxygen_spin_exchange' : 1,
'flag_radiation_pressure' : 1,
'flag_magnetic_field' : 1,
}];

cfg_filename_pattern = "test_set_2014_7_2_mac_%d";
output_filename_pattern = "%s";

cfg_file_directory = "../data/work/test_set_2014_7_2_mac";
```

Please note, if the value is like 185, please type as 185. with a '.' indicating it is a float. Sorry for the inconvenience.

*Name* | *Description* | *Data Type* | *Example*
------ | ------------- | ----------- | ----------
version| An arbitrary value given by user to identify/describe the whole simulation case | string | "1"
name | An arbitrary value for describing the simulation case | string | "simulate for Lijiang 2014" 
mass_sodium | mass of a single sodium atom (kg)| float | 
mass_nitrogen | mass of a single nitrogen atom (kg)| float |
temperature | temperature of the sodium layer that will be simulated (K)| float | 185.
thetas | collisional cross section between sodium atom and air molecules ($${number}/{m^3}$$) | float
K | Boltzmann constant (J/K)| float
nu_na | central frequency of sodium (Hz) | float
ns | number of air molecules ($${number}/{m^3}$$) | float
freq_fwhm_doppler_mhz | Doppler broadening profile's FWHM (MHz) | float
magnetic_field_related_list | this includes a list of dictionary for each case. for each case, two values are needed: `'time_larmor_precession_s'`, is the larmor precession period (s) for the geographical location at certain height, `'radian_emf_lb'`, is the angle between laser beam and the mangetic field inclination vector (radian). One can sepcify a list of this to simulate different angle, different location in one simulation run | list of dictionaries | [{'time_larmor_precession_s': 3.1072E-6,'radian_emf_lb': 2.2852}];` 
I_sat_list | A list of sodium atom saturation intensity, according to different papers, this could be either $$95.4W/m^2$$ or $$65.4W/m^2$$. ($$W/m^2$$) | list | [65.4]
intensity_list | this is the projected laser intensity at sodium layer, put a list of value you want to test here. ($$W/m^2$$) | list | logspace(log10(0.1), log10(10), num=50, base=10)
repetition_frequency_list | a list of repetition frequency. for a CW laser, this value equals to $$1/{simulation_time}$$ (Hz) | list | [600.]
pulse_format_file_list | a list of filename, file length to be tested. **Data in file should be saved in double precision**. The 'intensity_filename' is the filename for projected laser intensity data, the 'time_filename' is the filename for the timestamp for each laser intensity described in the intensity data file, the 'intensity_file_data_length' is the data's length of the previous two files. Their data length should be the same. | list of dictionaries | [{'intensity_filename' : '../data/const/measured_pulse/filtered_data_int_1.bin','time_filename' : '../data/const/measured_pulse/filtered_data_time_1.bin', 'intensity_file_datalength': 5333}]
spectrum_format_file_list | a list of filename, file length to be tested. **Data in file should be saved in double precision**. The 'spectrum_filename' is data file for power of spectrum (*will be normalized by with a total power of 1, so the relative shape of the spectrum is the most important*), the 'frequency_filename' is the data file for the frequency stamp of the spectrum shape described in the previous file, the 'idx_freq_length' is the length of the data. | list of dictionaries | [{'spectrum_filename' : '../data/const/spectrum/spectrum_power.bin','frequency_filename' : '../data/const/spectrum/spectrum_freq.bin','idx_freq_length' : 960}];
spectrum_simulation_parameter_list | this dictates the frequency range and fineness that the simulation will cover. 'freq_nu_start_mhz' is the start of the frequency range (float, MHz), 'freq_nu_end_mhz' is the end of the frequency range (float, MHz), 'freq_nu_incr_mhz' is the increment step for sweeping cross the frequency range (float, MHz) | list of dictionaries |
 

#### Running On A Single Machine
Originally, the software uses OpenMP for its local parallelization. But once I try to use it on my "updated" Maverick mac, the code failed compiling, since the "new" mac's clang compiler does not support OpenMP. 

While this is a wrecker, but it does not really mean you can not take adavantage of multi-core, multi-processor machine. On the contrary, one could use multi-process to run as many simulation instances at the same time as the number of physical cores you have without the overhead of multi-threading in the calculation.

A simple python script, `runcfg.py`, to run the code on multi-core, multi-processor machine is provided under the ./script folder. Open it and change the number of cores, and CFG directory, then call it by:

``
$ python runcfg.py
``

#### Running On A Cluster
We use openPBS as job resource manager. I am not sure the method to run the code  would be the same with other job resource manager, but the principle should be similar. 

What we want to do is quite similar to running the code on a single machine. We submit each simulation instance as a job to each node. And we submit as many as we could afford, then we loop through all the instances until everyone is finished. The PBS script `runcfg_PBS.sh` and `runcfg_PBS.py` is also provided under the ./script folder. To run it, first change the simulation code path in `runcfg_PBS.sh`, then run it by:

``
$ bash runcfg_PBS.sh
``

### Analyzing the simulation
#### Import data
The output data format strictly follows the rules of python library `configobj`. The name of the output data field is quite self-explanatory.

A ipython notebook `Import RSLT file.ipynb` for easy importing the data into pandas DataFrame is provided under the ./script folder.