from jinja2 import Template;
from glob import *;
import os;

tpl_filename = "../data/const/pbs_template/pbs_template.tpl";
tpl_fid = open(tpl_filename, 'r');
tpl_content = tpl_fid.read();
tpl_fid.close();

t = Template(tpl_content);

HH = "%02d"%(02);
MM = "%02d"%(00);
SS = "%02d"%(00);

PMEM = "1gb";

os.chdir('..');
BIN_DIR = os.getcwd()+"/bin";
os.chdir('script');
BIN_NAME = "./cp";

CFG_PATH = "../data/work/test_set_2014_7_24_ubc_pm";

cfg_filename_pattern = '*.cfg';
cfg_filename_search_pattern = CFG_PATH + '/' + cfg_filename_pattern;
cfg_filenames = glob(cfg_filename_search_pattern);

p = {};
p['HH'] = HH;
p['MM'] = MM;
p['SS'] = SS;
p['PMEM'] = PMEM;
p['BIN_DIR'] = BIN_DIR;
p['BIN_NAME'] = BIN_NAME;
p['CFG_PATH'] = CFG_PATH;

for cfg_filename in cfg_filenames:
	p['CFG_FILENAME'] = cfg_filename;
	pbs_filename = cfg_filename.replace('.cfg','.pbs');
	fid = open(pbs_filename, 'w');
	fid.write(t.render(p));
	fid.write('\n');
	fid.close();
	
