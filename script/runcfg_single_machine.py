import glob;
import multiprocessing;
import subprocess;
import sys;
import os;

cfg_path_name = '../data/work/test_set_2014_7_2_mac/';
cfg_file_pattern = 'test_set_2014_7_2_mac_*.cfg';

program_name = '../bin/cp';

# get CFG filenames
cfg_file_search_pattern = cfg_path_name + cfg_file_pattern;
filenames = glob.glob(cfg_file_search_pattern);

# get the number of core
# core_numbers = multiprocessing.cpu_count();
core_numbers = 2;

# function for calling the program
def caller(filename):
	command = os.getcwd()+ '/' + program_name;
	var = os.getcwd()+ '/' + filename;
	subprocess.call([command, var]);

# multi-processing
pool = multiprocessing.Pool(processes = core_numbers);
pool_outputs = pool.map(caller, filenames);
pool.close();
pool.join();