import glob;
import subprocess;
import os;

os.chdir('..');
pbs_path_name = os.getcwd() + '/' + 'data/work/test_set_2014_7_24_ubc_pm';
os.chdir('script');
pbs_file_pattern = 'test_set_2014_7_24_ubc_pm_*.pbs';
pbs_file_search_pattern = pbs_path_name + '/' + pbs_file_pattern;

pbs_filenames = glob.glob(pbs_file_search_pattern);

pbs_command = 'qsub';

for pbs_filename in pbs_filenames:
	subprocess.call([pbs_command, pbs_filename]); 


