from jinja2 import Template;
from numpy import *;
import time;
from datetime import datetime;
import urllib2;

tpl_filename = "../data/const/cfg_template/template_example.tpl";
tpl_fid = open(tpl_filename,'r');
tpl_content = tpl_fid.read();
tpl_fid.close();
t = Template(tpl_content);

version = "1";
name = "circular polarization UBC, 2013/aug/1x";
# really doubt that the following physical parameters needs to be changed ...
mass_sodium = 38.18E-27;
mass_nitrogen = 46.48E-27;
# well temperature could change, but 185K should be OK in most cases
temperature = 185.;
thetas = 5.93E-18;

K = 1.3806488E-23;
nu_na = 5.09E14;
ns = 1.359E19;
freq_fwhm_doppler_mhz = 1000.;

magnetic_field_related_list = [{'time_larmor_precession_s': 2.7269E-6, 
                                'radian_emf_lb': 2.8005}];
I_sat_list = [95.4];

laser_type_list = [1, 0];
intensity_list = logspace(log10(0.01), log10(10), num=1, base=10);
repetition_frequency_list = [600.];
laser_frequency_offset_mhz_list = linspace(0, 100, 30);

pulse_format_file_list = [{'intensity_filename' : '../data/const/measured_pulse/filtered_data_int_1.bin', 
                           'time_filename' : '../data/const/measured_pulse/filtered_data_time_1.bin', 
                           'intensity_file_datalength': 5333}];

spectrum_format_file_list = [{'spectrum_filename' : '../data/const/spectrum/spectrum_power.bin',
                              'frequency_filename' : '../data/const/spectrum/spectrum_freq.bin',
                              'idx_freq_length' : 960}];

spectrum_simulation_parameter_list = [{'freq_nu_start_mhz' : -1400.,
                                      'freq_nu_end_mhz' : 1400.,
                                      'freq_nu_incr_mhz' : 5.}];

EOM_list = [{'eom_flag':0, 'percentage':0.12},
	    {'eom_flag' : 1, 'percentage' : 0.12}
            ];

chirp_list = [{'flag_chirp' : 0,
               'chirp_rate_mhz' : 0.}];

random_generator_list = [{'flag_use_random_seed' : 1,
                          'seed_int' : 0,
                          'type' : 'gsl_rng_mt19937'}];

start_state_list = [{'flag_start_M_specific' : 0,
                     'start_M_level' : 7}];

trials_list = [200];
simulation_starttime_s_list = [0.];
simulation_duration_s_list = [200E-6];
time_delta_s = 30E-9;
step_per_mhz = 20;

print_each_step = 0;
print_photon_return = 1;
print_matrix = 0;
print_sm = 0;

simulation_flag_list = [{'flag_spin_exchange' : 1,
'flag_oxygen_spin_exchange' : 1,
'flag_radiation_pressure' : 1,
'flag_magnetic_field' : 1,
}];

cfg_filename_pattern = "test_set_2014_7_8_test_pm_%d";
output_filename_pattern = "%s";

#directory name is without trailing '/'
cfg_file_directory = "../data/work/test_set_2014_7_8_test_pm";

# for cfg file output
import os;
if os.path.exists(cfg_file_directory) == False:
    os.makedirs(cfg_file_directory);
    
p = {};
p['version'] = version;
p['name'] = name;

p['mass_sodium'] = mass_sodium;
p['mass_nitrogen'] = mass_nitrogen;

p['temperature'] = temperature;
p['thetas'] = thetas;

p['K'] = K;
p['nu_na'] = nu_na;
p['ns'] = ns;
p['freq_fwhm_doppler_mhz'] = freq_fwhm_doppler_mhz;

p['time_delta_s'] = time_delta_s;
p['step_per_mhz'] = step_per_mhz;

p['print_each_step'] = print_each_step;
p['print_photon_return'] = print_photon_return;
p['print_matrix'] = print_matrix;
p['print_sm'] = print_sm;

counter = 1;

for magnetic_field_related_item in magnetic_field_related_list:
    p['time_larmor_precession_s'] = magnetic_field_related_item['time_larmor_precession_s'];
    p['radian_emf_lb'] = magnetic_field_related_item['radian_emf_lb'];
    for I_sat in I_sat_list:
        p['I_sat'] = I_sat;
        for laser_type in laser_type_list:
        	p['laser_type'] = laser_type;
        	for intensity in intensity_list:
				p['intensity'] = intensity;
				for repetition_frequency in repetition_frequency_list:
					p['repetition_frequency'] = repetition_frequency;
					for pulse_format_file_item in pulse_format_file_list:
						p['intensity_filename'] = pulse_format_file_item['intensity_filename'];                
						p['time_filename'] = pulse_format_file_item['time_filename'];
						p['intensity_file_datalength'] = pulse_format_file_item['intensity_file_datalength'];
						for spectrum_format_file_item in spectrum_format_file_list:
							p['spectrum_filename'] = spectrum_format_file_item['spectrum_filename'];
							p['frequency_filename'] = spectrum_format_file_item['frequency_filename'];
							p['idx_freq_length'] = spectrum_format_file_item['idx_freq_length'];
							for spectrum_parameter_item in spectrum_simulation_parameter_list:
								p['freq_nu_start_mhz'] = spectrum_parameter_item['freq_nu_start_mhz'];
								p['freq_nu_end_mhz'] = spectrum_parameter_item['freq_nu_end_mhz'];
								p['freq_nu_incr_mhz'] = spectrum_parameter_item['freq_nu_incr_mhz'];
								for EOM_item in EOM_list:
									p['eom_flag'] = EOM_item['eom_flag'];
									p['percentage'] = EOM_item['percentage'];
									for chirp_item in chirp_list:
										p['flag_chirp'] = chirp_item['flag_chirp'];
										p['chirp_rate_mhz'] = chirp_item['chirp_rate_mhz'];
										for random_generator_item in random_generator_list:
											p['flag_use_random_seed'] = random_generator_item['flag_use_random_seed'];
											p['seed_int'] = random_generator_item['seed_int'];
											p['type'] = random_generator_item['type'];
											for laser_frequency_offset_mhz in laser_frequency_offset_mhz_list:
												p['laser_frequency_offset_mhz'] = laser_frequency_offset_mhz;
												for start_state_item in start_state_list:
													p['flag_start_M_specific'] = start_state_item['flag_start_M_specific'];
													p['start_M_level'] = start_state_item['start_M_level'];
													for trials in trials_list:
														p['trials'] = trials;
														for simulation_starttime_s in simulation_starttime_s_list:
															p['simulation_starttime_s'] = simulation_starttime_s;
															for simulation_duration_s in simulation_duration_s_list:
																p['simulation_duration_s'] = simulation_duration_s;
																for simulation_flag_item in simulation_flag_list:
																	p['flag_spin_exchange'] = simulation_flag_item['flag_spin_exchange'];
																	p['flag_oxygen_spin_exchange'] = simulation_flag_item['flag_oxygen_spin_exchange'];
																	p['flag_radiation_pressure'] = simulation_flag_item['flag_radiation_pressure'];
																	p['flag_magnetic_field'] = simulation_flag_item['flag_magnetic_field'];
																	# so we have looped over all parameters ... write them to file ...
																	filename = cfg_file_directory + '/' +cfg_filename_pattern%(counter) + '.cfg';
																	p['output_filename'] = cfg_file_directory + '/' + output_filename_pattern %(cfg_filename_pattern%(counter)) + '.rslt';
													
																	# write to the file
																	fid = open(filename, 'w');
																	fid.write(t.render(p));
																	fid.close();
																	counter = counter + 1;

