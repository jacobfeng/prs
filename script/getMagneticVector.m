latitude_deg = 26+42/60; 
longitude_deg = 100+1/60;
%
%UBC: W122deg 34min,N49deg 17min
%GaoMeiGu: E100deg 1min,N26deg 42min
%XingLong: E117deg 35min, N40deg 24min
%Mauna Kea: W155.4810,N19.8330, 4050m
%
dyear = decyear(2014,8,1);
epoch = '2010';
%height_series_m = linspace(0,100e3);
height_series_m = 90e3;

for i=1:length(height_series_m)
    [xyz, h, dec, dip, f] = wrldmagm(height_series_m(i), latitude_deg, longitude_deg, dyear, epoch);
    strength_nT(i) = f;
    inclination_deg(i) = dip;
end


%calculate precession period 
h_cross = 1.054573e-34;
miu_B = 9.274e-24;
B = strength_nT*1e-9;
tao_sec = 4*pi*h_cross./(miu_B*B);
