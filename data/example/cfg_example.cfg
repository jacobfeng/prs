# configuration file for photon return
# written by L. Feng
# 2014.6.9
# state: draft

version = "0.1";

name = "draft parameter file";

application:
{
	physics_constants:
	{
		# sodium mass for single atom, in unit of kg, 1.66E-27*23
		mass_sodium = 38.18E-27;
		# nitrogen mass for single molecule, in unit of kg, 1.66E-27*28
		mass_nitrogen = 46.48E-27;
		# temperature of sodium layer, in unit of Kelvin
		temperature = 185.;
		# cross section at 185K, in unit of m^2
		thetas = 72E-20;
		# Boltzmann constant
		K = 1.3806488E-23;
		# central frequency for sodium 
		nu_na = 5.09E14;
		# density of air molecule, atom/m^3, change this value according to http://omniweb.gsfc.nasa.gov/vitmo/msis_vitmo.html
		ns = 5.321e19;		
		# larmor precession period, in unit of s, 
		time_larmor_precession_s = 2.716E-6;
		# angle between magnetic vector and laser line of sight, in unit of radian
		radian_emf_lb = 2.8046;
		# Doppler broadening FWHM, in units of MHz,
		freq_fwhm_doppler_mhz = 1000.;
		# sodium saturation intensity, in units of W/m^2
		I_sat = 95.4;
	};
	
	laser_related:
	{
		# laser pulse format and spectral format are defined by files 
		# average intensity on sky, in unit of W/m^2
		intensity = 0.2256774;
		# repetition frequency, in unit of Hz
		repetition_frequency = 600.;
		# file name of the data file for instant intensity
		intensity_filename = "../data/const/flat_pulse/filtered_data_int.bin";
		# file name of the data file for time stamp of the instant intensity
		time_filename = "../data/const/flat_pulse/filtered_data_time.bin";
		# data length for the upper two files
		intensity_file_datalength = 5333;
		# file name of the data file for laser spectrum
		spectrum_filename = "../data/const/spectrum/spectrum_power.bin";
		# file name of the data file for frequency of the laser spectrum
		frequency_filename = "../data/const/spectrum/spectrum_freq.bin";
		# data length for the upper two files
		idx_freq_length = 960;
		# laser spectrum start simulation frequency, in unit of MHz
		freq_nu_start_mhz = -1400.;
		# laser spectrum end simulation frequency, in unit of MHz
		freq_nu_end_mhz = 1400.;	
		# laser spectrum increment frequency for simulation, in unit of MHz
		freq_nu_incr_mhz = 5.;
		
		EOM:
		{
			# enable EOM, 1 is on, 0 is off
			eom_flag = 1;   
			# percentage of how much power is branched to D2b side band
			percentage = 0.12;
		};
		
		chirp:
		{
			# flag to enable/disable chirping, 1 for chirping, 0 to disable
			flag_chirp = 0;
			# chirp rate, in units of MHz
			chirp_rate_mhz = 0.;
		}
	};
	
	simulation_related:
	{
		# this is related the the random generator used in the whole program
		random_generator_related:
		{
			# if it is 0, then it will use specific seed provided, if it is 1, it will use time() as the seed value
			flag_use_random_seed = 1;
			# random geneartor's seed value
			seed_int = 0;
			# random generator type, Mersenne Twister 2nd version would be enough, all other generators in this page is supported
			# http://www.gnu.org/software/gsl/manual/html_node/Random-number-generator-algorithms.html
			type = "gsl_rng_mt19937";
		};
		
		# decide which level an atom trial will be started from
		start_state:
		{
			# flag for choosing whether the start state is specific (1) or not (0)
			flag_start_M_specific = 0;
			# if specific, give the start state
			# this value should be 0-7 representing the F=1 M=-1,0,1; F=2 M=-2,-1,0,1,2 state respectively
			# if flag_start_M_specific is 0 this line will be ignored
			start_M_level = 7;
		};
		
		# numbers of atom that will be tested in each frequency bin, the more the better Monte Carlo convergence
		# but it will make the simulation slower
		trials = 60;
		
		# time step of the simulation, in unit of second
		time_delta_s = 30E-9;
		
		# how many frequency steps in each mhz bin
		# this value is used to determine how fine the convolution of atom lineshape to laser spectrum 
		step_per_mhz = 20;
		
		# flag for printing atom state in each step
		print_each_step = 0;
		
		# flag for printing photon return efficiency in the end
		print_photon_return = 1;
		
		# flag for printing transition matrix and saturation intensity matrix
		print_matrix = 0;
		
		# flag for printing magnetic state transition probability
		print_sm = 0;
		
		# output file name
		output_filename = "../data/example/cfg_run.rslt";
		
		simulation_flags:
		{
			# enable spin change simulation, 1 is enable, 0 is disable
			flag_spin_exchange = 1;
			# enable/disable spin exchange simulation of the oxygen molecule
			# the reason this is taken out as a single option:
			# oxygen only takes 1/5 of the air molecule, and only 1/2 chance of its collision with sodium
			# will cause a spin exchange. and its calculation would affect the calculation speed very much
			flag_oxygen_spin_exchange = 1;
			# enable radiation pressure simulation
			flag_radiation_pressure = 1;
			# enable magnetic field simulation
			flag_magnetic_field = 1;
		};
	};
};
