# configuration file for photon return
# written by L. Feng
# 2014.6.9
# state: draft

version = "{{version}}";

name = "{{name}}";

application:
{
	physics_constants:
	{
		# sodium mass for single atom, in unit of kg, 1.66E-27*23
		mass_sodium = {{mass_sodium}};
		# nitrogen mass for single molecule, in unit of kg, 1.66E-27*28
		mass_nitrogen = {{mass_nitrogen}};
		# temperature of sodium layer, in unit of Kelvin
		temperature = {{temperature}};
		# cross section at 185K, in unit of m^2
		thetas = {{thetas}};
		# Boltzmann constant
		K = {{K}};
		# central frequency for sodium 
		nu_na = {{nu_na}};
		# density of air molecule, atom/m^3, change this value according to http://omniweb.gsfc.nasa.gov/vitmo/msis_vitmo.html
		ns = {{ns}};		
		# larmor precession period, in unit of s, 
		time_larmor_precession_s = {{time_larmor_precession_s}};
		# angle between magnetic vector and laser line of sight, in unit of radian
		radian_emf_lb = {{radian_emf_lb}};
		# Doppler broadening FWHM, in units of MHz,
		freq_fwhm_doppler_mhz = {{freq_fwhm_doppler_mhz}};
		# sodium saturation intensity, in units of W/m^2
		I_sat = {{I_sat}};
	};
	
	laser_related:
	{
		# laser type, 0 for CW laser, 1 for pulse laser
		laser_type = {{laser_type}};
		# laser pulse format and spectral format are defined by files 
		# average intensity on sky, in unit of W/m^2
		intensity = {{intensity}};
		# repetition frequency, in unit of Hz
		repetition_frequency = {{repetition_frequency}};
		# laser frequency offset, in unit of mhz
		laser_frequency_offset_mhz = {{laser_frequency_offset_mhz}};
		# file name of the data file for instant intensity
		intensity_filename = "{{intensity_filename}}";
		# file name of the data file for time stamp of the instant intensity
		time_filename = "{{time_filename}}";
		# data length for the upper two files
		intensity_file_datalength = {{intensity_file_datalength}};
		# file name of the data file for laser spectrum
		spectrum_filename = "{{spectrum_filename}}";
		# file name of the data file for frequency of the laser spectrum
		frequency_filename = "{{frequency_filename}}";
		# data length for the upper two files
		idx_freq_length = {{idx_freq_length}};
		# laser spectrum start frequency, in unit of MHz
		freq_nu_start_mhz = {{freq_nu_start_mhz}};
		# laser spectrum end frequency, in unit of MHz
		freq_nu_end_mhz = {{freq_nu_end_mhz}};	
		# laser spectrum increment frequency, in unit of MHz
		freq_nu_incr_mhz = {{freq_nu_incr_mhz}};
		
		EOM:
		{
			# enable EOM, 1 is on, 0 is off
			eom_flag = {{eom_flag}};   
			# percentage of how much power is branched to D2b side band
			percentage = {{percentage}};
		};
		
		chirp:
		{
			# flag to enable/disable chirping, 1 for chirping, 0 to disable
			flag_chirp = {{flag_chirp}};
			# chirp rate, in units of MHz
			chirp_rate_mhz = {{chirp_rate_mhz}};
		}
	};
	
	simulation_related:
	{
		# this is related the the random generator used in the whole program
		random_generator_related:
		{
			# if it is 0, then it will use specific seed provided, if it is 1, it will use time() as the seed value
			flag_use_random_seed = {{flag_use_random_seed}};
			# random geneartor's seed value
			seed_int = {{seed_int}};
			# random generator type, Mersenne Twister 2nd version would be enough, all other generators in this page is supported
			# http://www.gnu.org/software/gsl/manual/html_node/Random-number-generator-algorithms.html
			type = "{{type}}";
		};
		
		# decide which level an atom trial will be started from
		start_state:
		{
			# flag for choosing whether the start state is specific (1) or not (0)
			flag_start_M_specific = {{flag_start_M_specific}};
			# if specific, give the start state
			# this value should be 0-7 representing the F=1 M=-1,0,1; F=2 M=-2,-1,0,1,2 state respectively
			# if flag_start_M_specific is 0 this line will be ignored
			start_M_level = {{start_M_level}};
		};
		
		# numbers of atom that will be tested in each frequency bin, the more the better Monte Carlo convergence
		# but it will make the simulation slower
		trials = {{trials}};
		
		# time step of the simulation, in unit of second
		time_delta_s = {{time_delta_s}};
		
		# time duration of the simulation
		simulation_duration_s = {{simulation_duration_s}};
		
		# start time of the simulation
		simulation_starttime_s = {{simulation_starttime_s}};
		
		# how many frequency steps in each mhz bin
		# this value is used to determine how fine the convolution of atom lineshape to laser spectrum 
		step_per_mhz = {{step_per_mhz}};
		
		# flag for printing atom state in each step
		print_each_step = {{print_each_step}};
		
		# flag for printing photon return efficiency in the end
		print_photon_return = {{print_photon_return}};
		
		# flag for printing transition matrix and saturation intensity matrix
		print_matrix = {{print_matrix}};
		
		# flag for printing magnetic state transition probability
		print_sm = {{print_sm}};
		
		# output file name
		output_filename = "{{output_filename}}";

		simulation_flags:
		{
			# enable spin change simulation, 1 is enable, 0 is disable
			flag_spin_exchange = {{flag_spin_exchange}};
			# enable/disable spin exchange simulation of the oxygen molecule
			# the reason this is taken out as a single option:
			# oxygen only takes 1/5 of the air molecule, and only 1/2 chance of its collision with sodium
			# will cause a spin exchange. and its calculation would affect the calculation speed very much
			flag_oxygen_spin_exchange = {{flag_oxygen_spin_exchange}};
			# enable radiation pressure simulation
			flag_radiation_pressure = {{flag_radiation_pressure}};
			# enable magnetic field simulation
			flag_magnetic_field = {{flag_magnetic_field}};
		};
	};
	# new parameters could be supported with extensible CFG template, use inheritance feature of Jinja2 
};

