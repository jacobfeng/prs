//****
//
// Code for Monte Carlo simulation of laser guide star photon-return
// 
// written by: Lu Feng, National Astronomical Observatories, Chinese Academy of Sciences
// version: 0.9
// date: 2012.10.24 
// contact: jacobfeng@gmail.com
// license information: refer to README "license" section
//
//****
// 
// This code is based on Prof. E. Kibblewhite's legacy code (Mathematica) for simulating Palomar laser guide star and with
// the aids from Prof. Kibblewhite for its completion. 
// Simulation process is based on rate equation of the sodium atom, and more details could be found in Prof. Kibblewhite's
// ESO study,  "Sodium laser guide star return flux study for the European Southern Observatory", 2008 and related papers.
//
//****
//
// Update Log
// Version 0.9, 2012.10.24: 
// basic function is complete, result needs to be further testified numerically
// need to add helper function for parameter input
// 
// Version 0.91, 2012.10.26:
// implement command line parsing codes
//
// Version 1, 2013, 12
// only thing missing is the spin exchange code
// 
// Version 1.1, 2014.1.22:
//
//****



//for Cpp
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <utility>  
#include <cmath>

//for C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h> 
#include <getopt.h>

// for libconfig
#include <libconfig.h>

// for GSL
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_integration.h>

#define atmatrix_r(row,column,columns) ((row)*(columns)+(column))
#define atmatrix_c(row,column,rows) ((column)*(rows)+(row))

double poch(double x, int n){
	// poch factorial
	double result = 1.;
	for(int i=0;i<n;i++){
		result *= (x+(double)i);
	}
	return result;
}

double jacobip(int n, double a, double b, double x){
	// jacobi polynomial deduced from the derivative equation format
	double result = 0;
    if(n==0){
        result = 1;
    }else if(n==1){
        result = .5*(a-b+x*(2+a+b));
    }else if(n==2){
        result = 1./8.*(-4+pow(a,2.)-b+pow(b,2.)-a*(1+2*b)+2*x*(3*a+pow(a,2.)-b*(3+b))+pow(x,2.)*(12+pow(a,2.)+7*b+pow(b,2.)+a*(7+2*b)));
    }else if(n==3){
        result = 1./48.*(pow(a,3.)-3*pow(a,2.)*(1+b)+b*(16+3*b-pow(b,2.))+a*(-16+3*pow(b,2.))+3*pow(x,2.)*(pow(a,3.)+pow(a,2.)*(9+b)-a*(-20+pow(b,2.))-b*(20+9*b+pow(b,2.)))+
            3*x*(-24+pow(a,3.)-pow(a,2.)*(-3+b)-10*b+3*pow(b,2.)+pow(b,3.)-a*(10+10*b+pow(b,2.)))+pow(x,3.)*(120+pow(a,3.)+74*b+15*pow(b,2.)+pow(b,3.)+3*pow(a,2.)*(5+b)+a*(74+30*b+3*pow(b,2.))));
    }else if(n==4){
        result = 1./384.*(144+pow(a,4.)+42*b-37*pow(b,2.)-6*pow(b,3.)+pow(b,4.)-2*pow(a,3.)*(3+2*b)+pow(a,2.)*(-37+6*b+6*pow(b,2.))+
            a*(42+86*b+6*pow(b,2.)-4*pow(b,3.))+6*pow(x,2.)*(-240+10*pow(a,3.)+pow(a,4.)-118*b+11*pow(b,2.)+10*pow(b,3.)+pow(b,4.)+pow(a,2.)*(11-14*b-2*pow(b,2.))-2*a*(59+49*b+7*pow(b,2.)))+
            4*x*(pow(a,4.)-2*pow(a,3.)*(-1+b)-pow(a,2.)*(37+18*b)-b*(-110-37*b+2*pow(b,2.)+pow(b,3.))+2*a*(-55+9*pow(b,2.)+pow(b,3.)))+
            4*pow(x,3.)*(pow(a,4.)+2*pow(a,3.)*(9+b)+pow(a,2.)*(107+18*b)-2*a*(-105+9*pow(b,2.)+pow(b,3.))-b*(210+107*b+18*pow(b,2.)+pow(b,3.)))+
            pow(x,4.)*(1680+pow(a,4.)+1066*b+251*pow(b,2.)+26*pow(b,3.)+pow(b,4.)+pow(a,3.)*(26+4*b)+pow(a,2.)*(251+78*b+6*pow(b,2.))+2*a*(533+251*b+39*pow(b,2.)+2*pow(b,3.))));    
    }else{
        fprintf(stderr, "Error: order not supported by jacobip()\n");
        exit(1);
    }
	return result;
}

void vxs_inv_d(int n, double * vector_a, double s, double * vector_b){
	// a is a double vector of length n, s is a scalar
	// this function computes result of 
	// vector_b = s./vector_a
	for(int i=0;i<n;i++){
		if(vector_a[i]!=0){
			vector_b[i] = s/vector_a[i];
		}
	}
	return;
}

double d(int f, int m, int i, double beta_radian){
	// function described in Edward's ESO review study, eq. 15
	double result = 0;
	if(beta_radian!=0){
		result = 
			sqrt((double)(gsl_sf_fact(f+i)*gsl_sf_fact(f-i)/gsl_sf_fact(f+m)/gsl_sf_fact(f-m)))
			*(pow(cos(beta_radian/2),i+m)) 
			*(pow(sin(beta_radian/2),i-m)) 
			*jacobip(f-i,i-m,i+m,cos(beta_radian));
	}
	return result;
}

double fx(double x, void * params){
	return exp(-x*x);
}

double psi(double x){
	gsl_function f;
	f.function=fx;
	double integration_result, integration_error;
	size_t n;
	gsl_integration_qng(&f, 0, x, 1e-10, 1e-2, &integration_result, &integration_error, &n);
	return x*exp(-x*x)+(2*x*x+1)*integration_result;
}

int main(int argc, char * argv[]){
	config_t cfg;
	
	// initialize Config environment
	config_init(&cfg);
	
	const char * readin_file_name_cfg = "../data/example/cfg_example.cfg";
	
	if(argc==2){
		readin_file_name_cfg = argv[1];
	}
	
	// configlib asking file path with backslash escaping
	if(!config_read_file(&cfg, readin_file_name_cfg)){
		fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
				config_error_line(&cfg), config_error_text(&cfg));
		config_destroy(&cfg);		
		return(EXIT_FAILURE);
	}
	
	const char * simulation_name = NULL;
	const char * simulation_version = NULL;
	
	config_lookup_string(&cfg, "name", &simulation_name);
	config_lookup_string(&cfg, "version", &simulation_version);

	// enable spin exchange simulation, 1 is enable, 0 is disable
	int flag_spin_exchange = 1;
	// enable/disable spin exchange simulation of the oxygen molecule
	// the reason this is taken out as a single option:
	// oxygen only takes 1/5 of the air molecule, and only 1/2 chance of its collision with sodium
	// will cause a spin exchange. and its calculation would affect the calculation speed very much
	int flag_oxygen_spin_exchange = 1;
	// enable radiation pressure simulation
	int flag_radiation_pressure = 1;
	// enable magnetic field simulation
	int flag_magnetic_field = 1;

	
	// read in, modify or leave it
	config_setting_t * simulation_flags_settings = config_lookup(&cfg, "application.simulation_related.simulation_flags");
	
	config_setting_lookup_int(simulation_flags_settings, "flag_spin_exchange", &flag_spin_exchange);
	config_setting_lookup_int(simulation_flags_settings, "flag_oxygen_spin_exchange", &flag_oxygen_spin_exchange);
	config_setting_lookup_int(simulation_flags_settings, "flag_radiation_pressure", &flag_radiation_pressure);
	config_setting_lookup_int(simulation_flags_settings, "flag_magnetic_field", &flag_magnetic_field);
	
	config_setting_t * physics_constants = config_lookup(&cfg, "application.physics_constants");
	
	// sodium mass for single atom, in unit of kg, 1.66E-27*23
	double mass_sodium = 38.18E-27;
	// nitrogen mass for single molecule, in unit of kg, 1.66E-27*28
	double mass_nitrogen = 46.48E-27;
	// temperature of sodium layer, in unit of Kelvin
	double temperature = 185;
	// cross section at 185K, in unit of m^2
	double thetas = 72E-20;
	// Boltzmann constant
	double K = 1.3806488E-23;
	// central frequency for sodium 
	double nu_na = 5.09E14;
	// density of air molecule, atom/m^3, change this value according to http://omniweb.gsfc.nasa.gov/vitmo/msis_vitmo.html
	double ns = 5.551e19;
	// use get_magnetic_vector_projected.m	
	double time_larmor_precession_s = 4.2811*1e-6;
 	// angle between earth's magnetic field and laser beam direction, in radian
	double radian_emf_lb = 2.2035;
	// define Doppler broadening linewidth
	double freq_fwhm_doppler_mhz = 1000;
	// define sodium saturation intensity, in units of W/m^2
	double I_sat = 95.4;
	
	// read in, modify or leave it
	config_setting_lookup_float(physics_constants, "mass_sodium", &mass_sodium);
	config_setting_lookup_float(physics_constants, "mass_nitrogen", &mass_nitrogen);
	config_setting_lookup_float(physics_constants, "temperature", &temperature);
	config_setting_lookup_float(physics_constants, "thetas", &thetas);
	config_setting_lookup_float(physics_constants, "K", &K);
	config_setting_lookup_float(physics_constants, "nu_na", &nu_na);
	config_setting_lookup_float(physics_constants, "ns", &ns);
	config_setting_lookup_float(physics_constants, "time_larmor_precession_s", &time_larmor_precession_s);
	config_setting_lookup_float(physics_constants, "radian_emf_lb", &radian_emf_lb);
	config_setting_lookup_float(physics_constants, "freq_fwhm_doppler_mhz", &freq_fwhm_doppler_mhz);
	config_setting_lookup_float(physics_constants, "I_sat", &I_sat);
	
	// calculate
	// relative mass would be 
	double relative_mass = mass_nitrogen/mass_sodium;
	
	// laser intensity in W/m^2. this is the average power,
	// please note that the read in power data is already normalized to have a unit power, 
	// so multiplying this value will scale the output power
	double intensity_w_m2 = 1; 
	// repetition frequency of the pulse laser, in unit of Hz
	double repetition_frequency_hz = 600;
	// data length for the upper two files
	int data_length = 5333;
	// file name of the data file for instant intensity
	const char * readin_file_name_int = "filtered_data_int.bin";
	// file name of the data file for time stamp of the instant intensity
	const char * readin_file_name_time = "filtered_data_time.bin";
	// data length for the upper two files
	int idx_freq_length = 960;	
	// file name of the data file for laser spectrum
	const char * readin_file_name_spectrum = "spectrum_power.bin.bin";
	// file name of the data file for frequency of the laser spectrum
	const char * readin_file_name_frequency = "spectrum_freq.bin";
	// laser spectrum start frequency, in unit of MHz
	double freq_nu_start_mhz = -1400;
	// laser spectrum end frequency, in unit of MHz
	double freq_nu_end_mhz = 1400;	
	// laser spectrum increment frequency, in unit of MHz
	double freq_nu_incr_mhz = 5;

	// read in, modify or leave it
	config_setting_t * laser_related_settings = config_lookup(&cfg, "application.laser_related");
	config_setting_lookup_float(laser_related_settings, "intensity", &intensity_w_m2);
	config_setting_lookup_float(laser_related_settings, "repetition_frequency", &repetition_frequency_hz);
	config_setting_lookup_string(laser_related_settings, "intensity_filename", &readin_file_name_int);
	config_setting_lookup_string(laser_related_settings, "time_filename", &readin_file_name_time);
	config_setting_lookup_int(laser_related_settings, "intensity_file_datalength", &data_length);
	config_setting_lookup_string(laser_related_settings, "spectrum_filename", &readin_file_name_spectrum);
	config_setting_lookup_string(laser_related_settings, "frequency_filename", &readin_file_name_frequency);
	config_setting_lookup_int(laser_related_settings, "idx_freq_length", &idx_freq_length);	
	config_setting_lookup_float(laser_related_settings, "freq_nu_start_mhz", &freq_nu_start_mhz);
	config_setting_lookup_float(laser_related_settings, "freq_nu_end_mhz", &freq_nu_end_mhz);
	config_setting_lookup_float(laser_related_settings, "freq_nu_incr_mhz", &freq_nu_incr_mhz);
	
	// calculate 
	// read in data file
	double * intensity_readin = NULL;
	double * time_readin = NULL;
	if(data_length == 0 || idx_freq_length == 0){
		fprintf(stderr, "data length can not be 0\n");
		return -1;
	}
	time_readin = (double *)malloc(sizeof(double)*data_length);
	intensity_readin = (double *)malloc(sizeof(double)*data_length);
	FILE * intensity_file_ptr = fopen(readin_file_name_int, "r+");
	if(intensity_file_ptr == NULL){
		fprintf(stderr, "intensity file for pulse format can not be opened.\n");
	}
	FILE * time_file_ptr = fopen(readin_file_name_time, "r+");
	if(time_file_ptr == NULL){
		fprintf(stderr, "time file for pulse format can not be opened.\n");
	}	// used for removing measurement offset, detail see to the UBC test data file	
	double t0=0; 

	fseek(intensity_file_ptr, 0, SEEK_SET); 
	fseek(time_file_ptr, 0, SEEK_SET);
	
	for(int i=0;i<data_length;i++){
		fread(&intensity_readin[i], sizeof(double), 1, intensity_file_ptr);
		fread(&time_readin[i], sizeof(double), 1, time_file_ptr);
		if(i==0){
			t0 = time_readin[0];
			time_readin[0] = 0;
		}else{
			time_readin[i] -= t0;
		}
	}
	fclose(intensity_file_ptr);
	fclose(time_file_ptr);

	// scaling instant intensity 
	for(int i=0;i<data_length;i++){
		intensity_readin[i] = intensity_readin[i] * intensity_w_m2;
	}	
			
	// actual simulation duration
	double time_simulation_s = time_readin[data_length-1];
	// should be simulation time for one pulse
	double simulation_time = 1/repetition_frequency_hz;

	// read in spectrum data
	double * int_laser = (double *)malloc(sizeof(double)*idx_freq_length);	
	double * freq_laser = (double *)malloc(sizeof(double)*idx_freq_length);
	double int_laser_total = 0;
	
	FILE * laser_spectrum_power_fptr = fopen(readin_file_name_spectrum,"r");
	if(laser_spectrum_power_fptr == NULL){
		fprintf(stderr, "power file for spectrum format can not be opened.\n");
	}
	FILE * laser_spectrum_freq_fptr = fopen(readin_file_name_frequency,"r");
	if(laser_spectrum_freq_fptr == NULL){
		fprintf(stderr, "frequency file for spectrum format can not be opened.\n");
	}	
	fseek(laser_spectrum_power_fptr, 0, SEEK_SET);
	fseek(laser_spectrum_freq_fptr, 0, SEEK_SET);

	for(int idx = 0; idx<idx_freq_length; idx++){
		fread(&int_laser[idx], sizeof(double), 1, laser_spectrum_power_fptr);
		fread(&freq_laser[idx], sizeof(double), 1, laser_spectrum_freq_fptr);
		int_laser_total += int_laser[idx];
	}
	fclose(laser_spectrum_power_fptr);
	fclose(laser_spectrum_freq_fptr);
	
	// enable EOM, 1 is on, 0 is off
	int flag_backpump = 1;
	// percentage of how much power is branched to D2b side band
	double backpump_fraction = 0.12;
	
	
	// read in, modify or leave it
	config_setting_t * eom_settings = config_lookup(&cfg, "application.laser_related.EOM");
	config_setting_lookup_int(eom_settings, "eom_flag", &flag_backpump);
	config_setting_lookup_float(eom_settings, "percentage", &backpump_fraction);
	
	double alpha = (double)flag_backpump*backpump_fraction;
	double intensity_normalized_factor = 1/(1+2*alpha); 
	
	// enable chirping, 1 is on, 0 is off
	int flag_chirp = 0;
	// percentage of how much power is branched to D2b side band
	double chirp_rate_mhz = 0;
	
	config_setting_t * chirp_settings = config_lookup(&cfg, "application.laser_related.chirp");
	config_setting_lookup_int(chirp_settings, "flag_chirp", &flag_chirp);
	config_setting_lookup_float(chirp_settings, "chirp_rate_mhz", &chirp_rate_mhz);
	
	chirp_rate_mhz *= (double)flag_chirp*(double)flag_radiation_pressure;
	
	// numbers of atom that will be tested in each frequency bin, the more the better Monte Carlo convergence
	// but it will make the simulation slower
	int trials = 60;
	// time step of the simulation, in unit of second
	double time_delta_s = 30*1e-9;		
	// how many frequency steps in each mhz bin
	// this value is used to determine how fine the convolution of atom lineshape to laser spectrum 
	int step_per_mhz = 20;
	// flag for print atom state in each step
	int flag_print_each_step = 0;
	// flag for print photon return efficiency in the end
	int flag_print_photon_return = 1;
	// flag for printing transition matrix and saturation intensity matrix
	int flag_print_matrix = 0;
	// flag for printing magnetic state transition probability
	int flag_print_sm = 0;
	// simulation result output file file name, currently unused
	const char * output_filename = NULL;
	
	// read in, modify or leave it
	config_setting_t * simulation_related_settings = config_lookup(&cfg, "application.simulation_related");
	config_setting_lookup_int(simulation_related_settings, "trials", &trials);
	config_setting_lookup_float(simulation_related_settings, "time_delta_s", &time_delta_s);
	config_setting_lookup_int(simulation_related_settings, "step_per_mhz", &step_per_mhz);
	config_setting_lookup_int(simulation_related_settings, "print_each_step", &flag_print_each_step);
	config_setting_lookup_int(simulation_related_settings, "print_photon_return", &flag_print_photon_return);
	config_setting_lookup_string(simulation_related_settings, "output_filename", &output_filename);
	config_setting_lookup_int(simulation_related_settings, "print_matrix", &flag_print_matrix);
	config_setting_lookup_int(simulation_related_settings, "print_sm", &flag_print_sm);
	
	const double time_cycle_time_s = 32*1e-9;
	if(time_delta_s >= time_cycle_time_s){
		fprintf(stderr, "Simulation time increment should be smaller than the smallest cycle time (32ns)\n");
		return 1;
	}
	int steps = round(time_simulation_s/time_delta_s);
	
	// random generator seed
	int random_generator_seed_int;
	int flag_use_random_seed = 1;
        unsigned long int random_generator_seed = (unsigned)time(NULL);
	// random generator type
	const char * generator_type_name;
	const gsl_rng_type * random_generator_type = gsl_rng_mt19937;
	
	// read in, modify or leave it
	config_setting_t * randgen_settings = config_lookup(&cfg, "application.simulation_related.random_generator_related");
	if(config_setting_lookup_int(randgen_settings, "seed_int", &random_generator_seed_int) == CONFIG_TRUE
	&& config_setting_lookup_int(randgen_settings, "flag_use_random_seed", &flag_use_random_seed) == CONFIG_TRUE
	&& flag_use_random_seed == 0){
		random_generator_seed = (unsigned long int)random_generator_seed_int;
	}
	
	if(config_setting_lookup_string(randgen_settings, "type", &generator_type_name)==CONFIG_TRUE){
		if(strcmp(generator_type_name, "gsl_rng_mt19937")){
			random_generator_type = gsl_rng_mt19937;
		}else if(strcmp(generator_type_name, "gsl_rng_ranlxs0")){
			random_generator_type = gsl_rng_ranlxs0;
		}else if(strcmp(generator_type_name, "gsl_rng_ranlxs1")){
			random_generator_type = gsl_rng_ranlxs1;
		}else if(strcmp(generator_type_name, "gsl_rng_ranlxs2")){
			random_generator_type = gsl_rng_ranlxs2;
		}else if(strcmp(generator_type_name, "gsl_rng_ranlxd1")){
			random_generator_type = gsl_rng_ranlxd1;
		}else if(strcmp(generator_type_name, "gsl_rng_ranlxd2")){
			random_generator_type = gsl_rng_ranlxd2;
		}else if(strcmp(generator_type_name, "gsl_rng_ranlux")){
			random_generator_type = gsl_rng_ranlux;
		}else if(strcmp(generator_type_name, "gsl_rng_ranlux389")){
			random_generator_type = gsl_rng_ranlux389;
		}else if(strcmp(generator_type_name, "gsl_rng_cmrg")){
			random_generator_type = gsl_rng_cmrg;
		}else if(strcmp(generator_type_name, "gsl_rng_mrg")){
			random_generator_type = gsl_rng_mrg;
		}else if(strcmp(generator_type_name, "gsl_rng_taus")){
			random_generator_type = gsl_rng_taus;
		}else if(strcmp(generator_type_name, "gsl_rng_taus2")){
			random_generator_type = gsl_rng_taus2;
		}else if(strcmp(generator_type_name, "gsl_rng_gfsr4")){
			random_generator_type = gsl_rng_gfsr4;
		}
	}
	
	// calculate
	gsl_rng * random_generator;
	gsl_rng_env_setup();
	random_generator = gsl_rng_alloc(random_generator_type);
	if(random_generator==NULL){
		fprintf(stderr, "can not allocate random number generator\n");
		exit(1);
	}
	// define random number seed
	gsl_rng_set(random_generator, random_generator_seed);

	// GSL interpolation environment setting
	gsl_interp_accel * pulse_data_acc = gsl_interp_accel_alloc();
	// cspline is used for interpolation
	gsl_spline * pulse_data_spline = gsl_spline_alloc(gsl_interp_cspline, data_length);
	
	//initialize spline
	gsl_spline_init(pulse_data_spline, time_readin, intensity_readin, data_length);	
	
	// flag for choosing whether the start state is specific (1) or not (0)
	int flag_start_M_specific = 0;
	// if specific, give the start state
	// this value should be 0-7 representing the F=1 M=-1,0,1; F=2 M=-2,-1,0,1,2 state respectively.
	int start_M_level = 3;
	
	// read in, modify or leave it
	config_setting_t * start_state_settings = config_lookup(&cfg, "application.simulation_related.start_state");
	
	if(config_setting_lookup_int(start_state_settings, "flag_start_M_specific", &flag_start_M_specific)==CONFIG_FALSE 
	|| flag_start_M_specific == 0){
		start_M_level = gsl_rng_uniform_int(random_generator,8);
	}else{
		config_setting_lookup_int(start_state_settings, "start_M_level", &start_M_level);
	}
	
	// index map for state <==> data storage	
	int idx_u_f_0_center = 0;
	int idx_u_f_1_center = 2;
	int idx_u_f_2_center = 6;
	int idx_u_f_3_center = 12;
	
	int idx_l_f_1_center = 17;
	int idx_l_f_2_center = 21;
		
	// define transition matrix for circular polarization
	int rows = 16; 
	int columns = 24;
	double * transition_matrix = (double *)malloc(sizeof(double)*rows*columns);
	double * saturation_intensity = (double *)malloc(sizeof(double)*rows*columns);
	double * saturation_parameter = (double *)malloc(sizeof(double)*rows*columns);
	
	for(int i=0;i<rows*columns;i++){
		transition_matrix[i] = 0;
		saturation_intensity[i] = 0;
		saturation_parameter[i] = 0;
	}
	
	// since this matrix will only be used to check down pumping parameters 
	transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] = 25;
	transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] = 15;
	transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center-1,columns)] = 20;
	transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_1_center-1,columns)] = 25;
	transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center-1,columns)] = 5;
	
	transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] = 20;
	transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] = 20;	
	transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+0,columns)] = 25;
	transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+0,columns)] = 15;
	
	transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] = 25;
	transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] = 15;	
	transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_1_center+1,columns)] = 30;

	transition_matrix[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)] = 20;
	transition_matrix[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)] = 20;
	transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-2,columns)] = 6;
	transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-2,columns)] = 10;
	transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-2,columns)] = 4;
		
	transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)] = 3;
	transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)] = 5;
	transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)] = 32;
	transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center-1,columns)] = 3;
	transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_2_center-1,columns)] = 15;
	transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center-1,columns)] = 12;
		
	transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)] = 4;
	transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)] = 36;
	transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+0,columns)] = 1;		
	transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+0,columns)] = 15;
	transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+0,columns)] = 24;
	
	transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)] = 4;
	transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)] = 5;
	transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)] = 32;
	transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+1,columns)] = 10;
	transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+1,columns)] = 40;

	transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)] = 20;
	transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)] = 20;
	transition_matrix[atmatrix_r(idx_u_f_3_center+3,idx_l_f_2_center+2,columns)] = 60;
	
	// define stauration intensity, w/m^2
	for(int i=0;i<rows*columns;i++){
		if(transition_matrix[i]!=0){
			saturation_intensity[i] = I_sat*60/transition_matrix[i];
		}
	}
	
	// calculate atom response to laser spectrum
	int freq_nu_length = (freq_nu_end_mhz-freq_nu_start_mhz)*step_per_mhz;
	double * freq_nu_x = (double *)malloc(sizeof(double)*freq_nu_length);
	for(int i=0;i<freq_nu_length;i++){
		freq_nu_x[i] = freq_nu_start_mhz+(double)i/step_per_mhz;
	}
	double * int_laser_atom = (double *)malloc(sizeof(double)*freq_nu_length);
	//sodium atom natural line width is 9.795Mhz
	const double sodium_atom_natural_linewidth_mhz = 9.795;
	for(int i=0;i<freq_nu_length;i++){
		double sum = 0;
		for(int j=0;j<idx_freq_length;j++){
			// convolve with Lorentzian function, which is defined by:
			// I(2theta) = w^2/(w^2 + (2theta-2theta0)^2)
			// where w is the FWHM of the peak
			// http://pd.chem.ucl.ac.uk/pdnn/peaks/loren.htm
			sum+=int_laser[j]/(1+pow(2*(freq_nu_x[i]-freq_laser[j])/sodium_atom_natural_linewidth_mhz,2.));
		}
		int_laser_atom[i] = sum/int_laser_total;
	}
	
	// check the integration value
	double value_check_sum_d = 0;
	for(int i=0;i<freq_nu_length;i++){
		value_check_sum_d += int_laser_atom[i];
	}
	if(fabs(value_check_sum_d/step_per_mhz-15.386)>1){
		fprintf(stderr, "Error: integrated value is too high = %f\n",value_check_sum_d);
		fprintf(stderr, "\t Integration of the response curve should be around 15.386\n");
		return 1;
	}
	
	// calculate probability of M state transition
	int n = time_larmor_precession_s/time_delta_s;
	double q[n];
	for(int i=0;i<n;i++){
		q[i] = i*time_delta_s/time_larmor_precession_s;
	}
	
	int total_f_number = 3;
	
	// row-wise allocation
	double *** sm = (double ***)malloc(sizeof(double **)*total_f_number);
	if(sm==NULL){
		fprintf(stderr, "Error: allocation failed for sm ** \n");
		exit(1);
	}
	for(int i=0;i<total_f_number;i++){
		sm[i] = (double **)malloc(sizeof(double *)*(2*(i+1)+1)*(2*(i+1)+1));
		if(sm==NULL){
			fprintf(stderr, "Error: allocation failed for sm * \n");
			exit(1);
		}
		for(int j=0;j<(2*(i+1)+1)*(2*(i+1)+1);j++){
			sm[i][j] = (double *)malloc(sizeof(double)*n);
			if(sm[i][j]==NULL){
				fprintf(stderr, "Error: allocation failed for sm \n");
				exit(1);
			}
			for(int t=0;t<n;t++){
				sm[i][j][t] = 0;
			}
		}
	}	
	
	for(int f=1;f<1+total_f_number-1;f++){
		for(int m_state=-f;m_state<(f+1);m_state++){
			for(int n_state=-f;n_state<(f+1);n_state++){
				gsl_complex * result_tmp = (gsl_complex *)malloc(sizeof(gsl_complex)*n);
				for(int j=0;j<n;j++){
					GSL_REAL(result_tmp[j])=0.; GSL_IMAG(result_tmp[j])=0.;
				}
				for(int j=0;j<n;j++){
					for(int i=-f;i<(f+1);i++){
						gsl_complex exponent;
						gsl_complex factor;
						GSL_SET_COMPLEX(&exponent, 0, -2*M_PI*i*(q[j]+.5));
						GSL_SET_COMPLEX(&factor, d(f, m_state, i, radian_emf_lb) * d(f, n_state, i, -1*radian_emf_lb),0);
						result_tmp[j] = gsl_complex_add(result_tmp[j], gsl_complex_mul(factor, gsl_complex_exp(exponent)));
					}
					sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j] = GSL_REAL(gsl_complex_mul(result_tmp[j],gsl_complex_conjugate(result_tmp[j])));
				}
			}
		}
	}

	// note the output format is compatible with python ConfigObj 4 library
	// ConfigObj 4 could be found here:
	// http://www.voidspace.org.uk/python/configobj.html
	FILE * fid;
	if(output_filename!=NULL){
		fid = fopen(output_filename, "w");
	}else{
		fid = stdout;
	}
	
	fprintf(fid, "\n[General]\n");
	fprintf(fid, "readin_file_name_cfg = %s\n", readin_file_name_cfg);
	fprintf(fid, "simulation_name = %s\n", simulation_name);
	fprintf(fid, "simulation_version = %s\n", simulation_version);
	
	fprintf(fid, "\t[[Simulation_flags]]\n");
	fprintf(fid, "\tflag_spin_exchange = %d\n", flag_spin_exchange);
	fprintf(fid, "\tflag_oxygen_spin_exchange = %d\n", flag_oxygen_spin_exchange);
	fprintf(fid, "\tflag_radiation_pressure = %d\n", flag_radiation_pressure);
	fprintf(fid, "\tflag_magnetic_field = %d\n", flag_magnetic_field);
	
	fprintf(fid, "\n[Physical_constants]\n");
	fprintf(fid, "mass_sodium = %E\n", mass_sodium);
	fprintf(fid, "mass_nitrogen = %E\n", mass_nitrogen);
	fprintf(fid, "temperature = %f\n", temperature);
	fprintf(fid, "thetas = %E\n", thetas);
	fprintf(fid, "K = %f\n", K);
	fprintf(fid, "nu_na = %E\n", nu_na);
	fprintf(fid, "ns = %E\n", ns);		
	fprintf(fid, "time_larmor_precession_s = %E\n", time_larmor_precession_s);	
	fprintf(fid, "radian_emf_lb = %f\n", radian_emf_lb);	
	fprintf(fid, "freq_fwhm_doppler_mhz = %E\n", freq_fwhm_doppler_mhz);	
	fprintf(fid, "I_sat = %f\n", I_sat);	
	
	fprintf(fid, "\n[Laser_related]\n");
	fprintf(fid, "intensity = %f\n", intensity_w_m2);
	fprintf(fid, "repetition_frequency = %f\n", repetition_frequency_hz);
	fprintf(fid, "intensity_filename = %s\n", readin_file_name_int);
	fprintf(fid, "time_filename = %s\n", readin_file_name_time);
	fprintf(fid, "intensity_file_datalength = %d\n", data_length);
	fprintf(fid, "spectrum_filename = %s\n", readin_file_name_spectrum);
	fprintf(fid, "frequency_filename = %s\n", readin_file_name_frequency);		
	fprintf(fid, "idx_freq_length = %d\n", idx_freq_length);
	fprintf(fid, "freq_nu_start_mhz = %f\n", freq_nu_start_mhz);
	fprintf(fid, "freq_nu_end_mhz = %f\n", freq_nu_end_mhz);
	fprintf(fid, "freq_nu_incr_mhz = %f\n", freq_nu_incr_mhz);
	fprintf(fid, "int_laser_atom_length = %d\n", freq_nu_length);
	// fprintf(fid, "int_laser_atom_freq = ");
	// for(int localcounter = 0; localcounter<freq_nu_length; localcounter++){
	// 	fprintf(fid, "%f,", freq_nu_x[localcounter]);
	// }
	// fprintf(fid, "\n");	
	// fprintf(fid, "int_laser_atom = ");
	// for(int localcounter = 0; localcounter<freq_nu_length; localcounter++){
	// 	fprintf(fid, "%f,", int_laser_atom[localcounter]);
	// }
	// fprintf(fid, "\n");
	
	fprintf(fid, "\t[[EOM]]\n");
	fprintf(fid, "\teom_flag = %d\n", flag_backpump);
	fprintf(fid, "\tpercentage = %f\n", backpump_fraction*100);
	fprintf(fid, "\talpha = %f\n", alpha);
	fprintf(fid, "\tintensity_normalized_factor = %f\n", intensity_normalized_factor);
	
	fprintf(fid, "\t[[Chirp]]\n");
	fprintf(fid, "\tflag_chirp = %d\n", flag_chirp);
	fprintf(fid, "\tchirp_rate_mhz = %f\n", chirp_rate_mhz);
	
	fprintf(fid, "\n[Simulation_related]\n");
	fprintf(fid, "trials = %d\n", trials);
	fprintf(fid, "time_delta_s = %E\n", time_delta_s);
	fprintf(fid, "step_per_mhz = %d\n", step_per_mhz);
	fprintf(fid, "flag_print_each_step = %d\n", flag_print_each_step);
	fprintf(fid, "flag_print_photon_return = %d\n", flag_print_photon_return);	
	fprintf(fid, "flag_print_sm = %d\n", flag_print_sm);
	fprintf(fid, "flag_print_matrix = %d\n", flag_print_matrix);
	fprintf(fid, "output_filename = %s\n", output_filename);	
	fprintf(fid, "freq_nu_start_mhz = %f\n", freq_nu_start_mhz);
	fprintf(fid, "freq_nu_end_mhz = %f\n", freq_nu_end_mhz);
	fprintf(fid, "freq_nu_incr_mhz = %f\n", freq_nu_incr_mhz);
	fprintf(fid, "steps = %d\n", steps);
	
	fprintf(fid, "\t[[Random_generator_related]]\n");
	fprintf(fid, "\tflag_use_random_seed = %d\n", flag_use_random_seed);
	fprintf(fid, "\tseed_int = %ld\n", random_generator_seed);
	fprintf(fid, "\ttype = %s\n", generator_type_name);
	
	fprintf(fid, "\t[[Start_state]]\n");
	fprintf(fid, "\tflag_start_M_specific = %d\n", flag_start_M_specific);
	fprintf(fid, "\tstart_M_level = %d\n", start_M_level);
	
	fprintf(fid, "\n[Results]\n");
	if(flag_print_matrix == 1){
		fprintf(fid, "\t[[saturation_intensity]]\n");
		fprintf(fid, "\tsaturation_intensity_rows = %d\n", rows);
		fprintf(fid, "\tsaturation_intensity_cols = %d\n", columns);
		fprintf(fid, "\tsaturation_intensity = ");
		for(int i=0;i<rows;i++){
			for(int j=0;j<columns;j++){
				fprintf(fid, "%f, ", saturation_intensity[atmatrix_r(i,j,columns)]); 
			}
		}
		fprintf(fid, "\n");

		fprintf(fid, "\t[[transition_matrix]]\n");
		fprintf(fid, "\ttransition_matrix_rows = %d\n", rows);
		fprintf(fid, "\ttransition_matrix_cols = %d\n", columns);
		fprintf(fid, "\ttransition_matrix = ");
		for(int i=0;i<rows;i++){
			for(int j=0;j<columns;j++){
				fprintf(fid, "%f, ", transition_matrix[atmatrix_r(i,j,columns)]); 
			}
		}	
		fprintf(fid, "\n");
	}
	
	if(flag_print_sm == 1){
		fprintf(fid, "\t[[sm]]\n");
		fprintf(fid, "\t\tf, m_state, n_state, j, sm\n");
		for(int f=1;f<1+total_f_number-1;f++){
			for(int m_state=-f;m_state<(f+1);m_state++){
				for(int n_state=-f;n_state<(f+1);n_state++){
					for(int j=0;j<n;j++){
						fprintf(fid, "\t\t%d, %d, %d, %d, %f\n", f, m_state, n_state, j, sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j]);
					}
				}
			}
		}
	}
	
	// Monte Carlo
	std::map<double, std::pair<double, double> > spectrum, spectrum1;
	std::map<double, double> nufinal, upperpop, lowerpop, photonreturn, gausupperpop,gauslowerpop, gausphotonreturn, velinc;

	double max_photon = 0;
	double average_photon = 0;
	
	int z_length = (int)((freq_nu_end_mhz-freq_nu_start_mhz)/freq_nu_incr_mhz); 

	for(int zzz=0;zzz<z_length;zzz++){
		double z = freq_nu_start_mhz+zzz*freq_nu_incr_mhz;
		// number of transition in each velocity group at each ground state
		int nnp[8]; 
		// number of photon returns in each trial
		double rr[trials]; 
		// set number of transition in upper and lower levels to zero
		int nnupper=0, nnlower=0; 
		 // set the average red shift of the starting velocity group to zero
		double nur=0;
		
		memset(nnp, 0, sizeof(int)*8);
		memset(rr, 0, sizeof(double)*trials);
		
		for(int ii=0;ii<trials;ii++){		
			// loop through each atom in the same velocity group
			int flag_current_m_state; //represent which state is the "lucky one"
			// set the initial M state
			if(flag_start_M_specific == 1){
				flag_current_m_state = start_M_level;
			}else{
				flag_current_m_state = gsl_rng_uniform_int(random_generator,8);
			}
			
			// number of photons absorbed by the state for this trial
			int np[8]; 
			// photonreturn backscattered by the state for this trial
			double r[8]; 
			memset(np, 0, sizeof(int)*8);
			memset(r, 0, sizeof(double)*8);

			// radiation pressure will cause the final frequency offset, this value bookkeep the final frequency for each atom
			double nu=z; 
			// this value is related to chirping
			double nu0 = 0;
			
			int ix = 0;
			// start of a single atom
			
			// velocity of sodium atom, angle angle_sodium_laser is between Na atom velocity and laser beam
			double velocity_sodium_pre = 457*pow((-log(gsl_rng_uniform_pos(random_generator))),0.4);
			double angle_sodium_laser = acos(2*(gsl_rng_uniform_pos(random_generator)-0.5));
			double velocity_sodium_after = velocity_sodium_pre;
	
			int particle_collision_count = 0;
			
			while(ix<steps){
				// kp is the counter for inner loop			
				int kp = steps-ix; 
				// p is the counter beginning from the first non-hit moment
				int p = 0; 
				// pk is the index for larmor precession cycle
				int pk = 0; 

				// the D2b repumping is controlled directly by EOM flag, the assumption here is that the laser linewidth is much lower than D2a D2b separation, 1.7GHz,
				// EOM is assumed to shift the whole laser spectrum from D2a center to D2b center
				// freq_offset_mhz defines transition frequency difference for l_1_u_0, l_1_u_1, l_1_u_2, l_2_u_1, l_2_u_2, l_2_u_3
				double freq_offset_mhz[6] = {-50.15, -34.34, 0., 
											 -92.67, -58.33, 0.}; 

				unsigned idx_l_1_center = 1;
				unsigned idx_l_2_center = 5;				
				
				switch(flag_current_m_state){
					case 0:{
					// F=1 M=-1
						while(p<kp){
							double time_current_s = ix*time_delta_s; 
							if(ix>=steps || time_current_s>time_readin[data_length-1]){
								break;  
							}
							double intensity_current_w_m2 = gsl_spline_eval(pulse_data_spline, time_current_s, pulse_data_acc); 
							// define total intensity at mesosphere in watts/m^2
							double intensity = intensity_current_w_m2*intensity_normalized_factor; 
							double intensity_backpump = intensity*alpha;

							// define conversion rate for power/arcsec <- photon_number/sec
							// 8192 = 2.7622E-15/(h*miu)
							double magic_number_backpump = 8192.33 * time_delta_s * intensity_backpump;
							
							// calculate saturation parameters, laster intensity/saturation intensity
							for(int i=0;i<rows*columns;i++){
								if(saturation_intensity[i]!=0){
									saturation_parameter[i] = intensity/saturation_intensity[i];
								}
							}

							saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							
							velocity_sodium_pre = velocity_sodium_after;
							double collision_probability = 2*K*temperature*ns*thetas/sqrt(M_PI)/mass_nitrogen/velocity_sodium_pre*psi(velocity_sodium_pre*sqrt(mass_nitrogen/(2*K*temperature)))*time_delta_s;
							bool collided = (collision_probability>gsl_rng_uniform_pos(random_generator));	
							double collision_doppler_mhz = 0;
							if(collided && flag_spin_exchange == 1){
								particle_collision_count += 1;
								particle_collision_count  = particle_collision_count % 10;

								// velocity of nitrogen atom, angle angel_sodium_nitrogen is between Na atom velocity and N2 atom velocity
								double velocity_nitrogen = 415*pow((-log(gsl_rng_uniform_pos(random_generator))),0.4);
								double angel_sodium_nitrogen = acos(2*(gsl_rng_uniform_pos(random_generator)-0.5));
								// relative velocity between two particles
								double velocity_relative_sodium_nitrogen = sqrt(pow(velocity_sodium_pre,2)+pow(velocity_nitrogen,2)-2*velocity_sodium_pre*velocity_nitrogen*cos(angel_sodium_nitrogen));
								double angel_relative_sodium_nitrogen = pow(velocity_nitrogen,2.)>=(pow(velocity_sodium_pre,2.)+pow(velocity_relative_sodium_nitrogen,2.))? M_PI-asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen):asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen);//calculate relative velocity direction respect to sodium atom
								
								// hard sphere interaction in CM reference frame
								double xx_hs = pow(gsl_rng_uniform_pos(random_generator),0.5);
								double thetacm_hs = 2*acos(xx_hs);
								double dvnacm_hs = (relative_mass/(1+relative_mass))*velocity_relative_sodium_nitrogen;
								double dvna_hs = 2*dvnacm_hs*sin(thetacm_hs/2);
								double theta_hs = atan(sin(thetacm_hs)/(1-cos(thetacm_hs)));
								double psi_hs = 2*M_PI*gsl_rng_uniform_pos(random_generator);
								double dvna1_hs = dvna_hs*pow(pow(cos(theta_hs),2)+pow(sin(theta_hs)*cos(psi_hs),2),0.5);
								double theta1_hs = atan(sin(theta_hs)*cos(psi_hs)/cos(theta_hs));
								double phi_hs = angel_relative_sodium_nitrogen + theta1_hs;
								
								velocity_sodium_after = sqrt((pow(velocity_sodium_pre,2.0)+pow(dvna1_hs,2.0)-2*velocity_sodium_pre*dvna1_hs*cos(phi_hs))+pow(dvna_hs*sin(theta_hs)*sin(psi_hs),2.0));
								double beta1 = 2*M_PI*gsl_rng_uniform_pos(random_generator)+atan(dvna_hs*sin(theta_hs)*sin(psi_hs)/dvna1_hs);
								double dalfa =  acos((pow(velocity_sodium_after,2.)+pow(velocity_sodium_pre,2.0)-pow(dvna_hs,2.))/(2*velocity_sodium_after*velocity_sodium_pre));
								angle_sodium_laser = acos(cos(angle_sodium_laser)*cos(dalfa)+sin(angle_sodium_laser)*sin(dalfa)*cos(M_PI-beta1));
								collision_doppler_mhz = velocity_sodium_after*cos(angle_sodium_laser)*nu_na/(2.998*1E8)/1e6;
								if(particle_collision_count==0&&flag_oxygen_spin_exchange){
									// this is going to hit with an oxygen particle.
									// 1 in 5 in air is oxygen, spin exchange by 1/2, so 1 in 10s collision will be for oxygen
									double trial_possibility = gsl_rng_uniform_pos(random_generator);	
									if(flag_current_m_state==0){
										if(trial_possibility<=1/16){flag_current_m_state=1;break;}
										if(trial_possibility<=1/8 && trial_possibility>1/16){flag_current_m_state=5;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=3;break;}
										// or it may just stay in this state
									}else if(flag_current_m_state==1){
										if(trial_possibility<=1/16){flag_current_m_state=2;break;}
										if(trial_possibility<=1/4 && trial_possibility>1/16){flag_current_m_state=6;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/4){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=4;break;}
										if(trial_possibility<=3/4 && trial_possibility>11/16){flag_current_m_state = 0;break;}
									}else if(flag_current_m_state==2){
										if(trial_possibility<=3/8){flag_current_m_state=7;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=6;break;}
										if(trial_possibility<=10/16 && trial_possibility>9/16){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=1;break;}
									}else if(flag_current_m_state==3){
										if(trial_possibility<=1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/8){flag_current_m_state=0;break;}
									}else if(flag_current_m_state==4){
										if(trial_possibility<=3/16){flag_current_m_state=5;break;}
										if(trial_possibility<=3/8 && trial_possibility>3/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=0;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=3;break;}
									}else if(flag_current_m_state==5){
										if(trial_possibility<=3/16){flag_current_m_state=6;break;}
										if(trial_possibility<=4/16 && trial_possibility>3/16){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>4/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>8/16){flag_current_m_state=0;break;}
										if(trial_possibility<=12/16 && trial_possibility>9/16){
										flag_current_m_state=4;break;}
									}else if(flag_current_m_state==6){
										if(trial_possibility<=1/8){flag_current_m_state=7;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>5/16){flag_current_m_state=1;break;}
										if(trial_possibility<=11/16 && trial_possibility>8/16){flag_current_m_state=5;break;}
									}else if(flag_current_m_state==7){
										if(trial_possibility<=1/8){flag_current_m_state=6;break;}
										if(trial_possibility<=4/8 && trial_possibility>1/8){flag_current_m_state=2;break;}
									}
								}								
							}
				
							int freq_idx[6];
							double intensity_partial[6];
							for(int i=0;i<6;i++){
								freq_idx[i] = lround((nu-nu0+collision_doppler_mhz+freq_offset_mhz[i]-freq_nu_start_mhz)*step_per_mhz);
								if((freq_idx[i]<0)||(freq_idx[i]>=freq_nu_length)){
									intensity_partial[i] = 0;
								}else{
									intensity_partial[i] = int_laser_atom[freq_idx[i]];
								}
							}
							
							double xsect[8];
							int xsect_idx_l_1_start = 2;   
							int xsect_idx_l_2_start = 5;  
							
							// considering saturation intensity, this step calculate the cross section that the laser will hit a sodium atom
							// ground level F=1, M=1, transition_matrix is defined as upper state -> lower state, 
							// calculation is done by Edward, "ESO study", eq 26.
							// transition to lower F=1,M=1 from F'=1, M=1
							double xsect_13 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)]));
							//transition from lower F=1,M=1 to F'=2, M=1
							double xsect_23 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)]));
							xsect[2] = xsect_13 + xsect_23;															
							//transition from lower F=1,M=0 to F'=0, M=0							
							double xsect_02 = transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-2]/(1+intensity_partial[xsect_idx_l_1_start-2]*saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)]));
							//transition from lower F=1,M=0 to F'=2, M=0
							double xsect_22 = transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)]));
							xsect[1] = xsect_02 + xsect_22;															
							//transition from lower F=1,M=-1 to F'=1, M=-1							
							double xsect_11 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)]));
							//transition from lower F=1,M=-1 to F'=2, M=-1
							double xsect_21 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)]));
							xsect[0] = xsect_11 + xsect_21;	
														
							//ground level F=2, M=2
							//transition from lower F=2,M=2 to F'=2, M=2							
							double xsect_28 = transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)]));
							//transition from lower F=2,M=2 to F'=3, M=2
							double xsect_38 = transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)]));
							xsect[7] = xsect_28 + xsect_38;															
							
							//ground level F=2, M=1
							//transition from lower F=2,M=1 to F'=1, M=1							
							double xsect_17 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=2, M=1							
							double xsect_27 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=3, M=1
							double xsect_37 = transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)]));
							xsect[6] = xsect_17 + xsect_27 + xsect_37;															
							
							//ground level F=2, M=0
							//transition from lower F=2,M=0 to F'=1, M=0							
							double xsect_16 = transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)]));
							//transition from lower F=2,M=0 to F'=3, M=0
							double xsect_36 = transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)]));
							xsect[5] = xsect_16 + xsect_36;															
							
							//ground level F=2, M=-1
							//transition from lower F=2,M=-1 to F'=1, M=-1							
							double xsect_15 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=2, M=-1							
							double xsect_25 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=3, M=-1
							double xsect_35 = transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)]));
							xsect[4] = xsect_15 + xsect_25 + xsect_35;															

							//ground level F=2, M=-2
							//transition from lower F=2,M=-2 to F'=2, M=-2							
							double xsect_24 = transition_matrix[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)]));
							//transition from lower F=2,M=-2 to F'=3, M=-2
							double xsect_34 = transition_matrix[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)]));
							xsect[3] = xsect_24 + xsect_34;			

							// first we check whether it will absorb a photon or not
							ix+=1; 
 							pk = p%n;  
							if(flag_magnetic_field==0){
								pk = 0;  
							}
							// sm indexing is defined as sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j]
							double state_xsect = (xsect[idx_l_1_center-1]*sm[1-1][(-1+1)*(2*1+1)+(1-1)][pk]) 
									+(xsect[idx_l_1_center+0]*sm[1-1][(-1+1)*(2*1+1)+(1+0)][pk])
									+(xsect[idx_l_1_center+1]*sm[1-1][(-1+1)*(2*1+1)+(1+1)][pk]);											
							double prob_xsect = state_xsect * magic_number_backpump;
							double prob_current = gsl_rng_uniform_pos(random_generator);
							
							if(prob_current<prob_xsect){
								// if it absorb a photon, then a radiation pressure caused red shift will happen
								// radiation pressure caused red shift
								nu += 0.05*flag_radiation_pressure; 
								double y = gsl_rng_uniform_pos(random_generator);
								// select which upper level gets chosen, also considering the magnetic fiedl effect
								double yy_68_2 =           sm[1-1][(-1+1)*(2*1+1)+(1+1)][pk]*xsect_23/xsect[2];
								double yy_67_2 = yy_68_2 + sm[1-1][(-1+1)*(2*1+1)+(0+1)][pk]*xsect_22/xsect[1];
								double yy_66_2 = yy_67_2 + sm[1-1][(-1+1)*(2*1+1)+(-1+1)][pk]*xsect_21/xsect[0];
								double yy_68_1 = yy_66_2 + sm[1-1][(-1+1)*(2*1+1)+(1+1)][pk]*xsect_13/xsect[2];
								double yy_66_1 = yy_68_1 + sm[1-1][(-1+1)*(2*1+1)+(-1+1)][pk]*xsect_11/xsect[0];
								double yy_67_0 = yy_66_1 + sm[1-1][(-1+1)*(2*1+1)+(0+1)][pk]*xsect_02/xsect[1];
								//check which magnetic state it will decay into by earth magnetic field, and from there optical pump to an upper state, and back to another ground state
								if(y<yy_68_2){
									//upper state is (2,1)
									//check which ground state it will fall back to
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<15){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<20){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_67_2){
									//upper state is 2,0
									//transit from F=2,M=2, to F=2,M=1
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<30){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<55){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_66_2){
									//transit from F=2,M=2, to F=2, M=0
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;	
									}else if(yr<45){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;	
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;  
									}
									break;
								}else if(y<yy_68_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<1){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<4){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_66_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<6){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<9){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1; 
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_67_0){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<40){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}
							}else{
								p += 1;
							}					
						}
					break;}
					case 1:{
						// F=1 M=0
						while(p<kp){
							double time_current_s = ix*time_delta_s;  
							if(ix>=steps || time_current_s>time_readin[data_length-1]){
								break;  
							}
							double intensity_current_w_m2 = gsl_spline_eval(pulse_data_spline, time_current_s, pulse_data_acc); 
							// define total intensity at mesosphere in watts/m^2
							double intensity = intensity_current_w_m2*intensity_normalized_factor;  
							double intensity_backpump = intensity*alpha;

							// define conversion rate for power/arcsec <- photon_number/sec
							double magic_number_backpump = 8192.33 * time_delta_s * intensity_backpump;
							
							// calculate saturation parameters, laster intensity/saturation intensity
							for(int i=0;i<rows*columns;i++){
								if(saturation_intensity[i]!=0){
									saturation_parameter[i] = intensity/saturation_intensity[i];
								}
							}

							saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							
							velocity_sodium_pre = velocity_sodium_after;
							double collision_probability = 2*K*temperature*ns*thetas/sqrt(M_PI)/mass_nitrogen/velocity_sodium_pre*psi(velocity_sodium_pre*sqrt(mass_nitrogen/(2*K*temperature)))*time_delta_s;
							bool collided = (collision_probability>gsl_rng_uniform_pos(random_generator));	
							double collision_doppler_mhz = 0;
							if(collided && flag_spin_exchange == 1){
								particle_collision_count += 1;
								particle_collision_count  = particle_collision_count % 10;

								// velocity of nitrogen atom, angle angel_sodium_nitrogen is between Na atom velocity and N2 atom velocity
								double velocity_nitrogen = 415*pow((-log(gsl_rng_uniform_pos(random_generator))),0.4);
								double angel_sodium_nitrogen = acos(2*(gsl_rng_uniform_pos(random_generator)-0.5));
								// relative velocity between two particles
								double velocity_relative_sodium_nitrogen = sqrt(pow(velocity_sodium_pre,2)+pow(velocity_nitrogen,2)-2*velocity_sodium_pre*velocity_nitrogen*cos(angel_sodium_nitrogen));
								double angel_relative_sodium_nitrogen = pow(velocity_nitrogen,2.)>=(pow(velocity_sodium_pre,2.)+pow(velocity_relative_sodium_nitrogen,2.))? M_PI-asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen):asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen);//calculate relative velocity direction respect to sodium atom
								
								// hard sphere interaction in CM reference frame
								double xx_hs = pow(gsl_rng_uniform_pos(random_generator),0.5);
								double thetacm_hs = 2*acos(xx_hs);
								double dvnacm_hs = (relative_mass/(1+relative_mass))*velocity_relative_sodium_nitrogen;
								double dvna_hs = 2*dvnacm_hs*sin(thetacm_hs/2);
								double theta_hs = atan(sin(thetacm_hs)/(1-cos(thetacm_hs)));
								double psi_hs = 2*M_PI*gsl_rng_uniform_pos(random_generator);
								double dvna1_hs = dvna_hs*pow(pow(cos(theta_hs),2)+pow(sin(theta_hs)*cos(psi_hs),2),0.5);
								double theta1_hs = atan(sin(theta_hs)*cos(psi_hs)/cos(theta_hs));
								double phi_hs = angel_relative_sodium_nitrogen + theta1_hs;
								
								velocity_sodium_after = sqrt((pow(velocity_sodium_pre,2.0)+pow(dvna1_hs,2.0)-2*velocity_sodium_pre*dvna1_hs*cos(phi_hs))+pow(dvna_hs*sin(theta_hs)*sin(psi_hs),2.0));
								double beta1 = 2*M_PI*gsl_rng_uniform_pos(random_generator)+atan(dvna_hs*sin(theta_hs)*sin(psi_hs)/dvna1_hs);
								double dalfa =  acos((pow(velocity_sodium_after,2.)+pow(velocity_sodium_pre,2.0)-pow(dvna_hs,2.))/(2*velocity_sodium_after*velocity_sodium_pre));
								angle_sodium_laser = acos(cos(angle_sodium_laser)*cos(dalfa)+sin(angle_sodium_laser)*sin(dalfa)*cos(M_PI-beta1));
								collision_doppler_mhz = velocity_sodium_after*cos(angle_sodium_laser)*nu_na/(2.998*1E8)/1e6;
								if(particle_collision_count==0&&flag_oxygen_spin_exchange){
									// this is going to hit with an oxygen particle.
									// 1 in 5 in air is oxygen, spin exchange by 1/2, so 1 in 10s collision will be for oxygen
									double trial_possibility = gsl_rng_uniform_pos(random_generator);	
									if(flag_current_m_state==0){
										if(trial_possibility<=1/16){flag_current_m_state=1;break;}
										if(trial_possibility<=1/8 && trial_possibility>1/16){flag_current_m_state=5;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=3;break;}
										// or it may just stay in this state
									}else if(flag_current_m_state==1){
										if(trial_possibility<=1/16){flag_current_m_state=2;break;}
										if(trial_possibility<=1/4 && trial_possibility>1/16){flag_current_m_state=6;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/4){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=4;break;}
										if(trial_possibility<=3/4 && trial_possibility>11/16){flag_current_m_state = 0;break;}
									}else if(flag_current_m_state==2){
										if(trial_possibility<=3/8){flag_current_m_state=7;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=6;break;}
										if(trial_possibility<=10/16 && trial_possibility>9/16){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=1;break;}
									}else if(flag_current_m_state==3){
										if(trial_possibility<=1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/8){flag_current_m_state=0;break;}
									}else if(flag_current_m_state==4){
										if(trial_possibility<=3/16){flag_current_m_state=5;break;}
										if(trial_possibility<=3/8 && trial_possibility>3/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=0;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=3;break;}
									}else if(flag_current_m_state==5){
										if(trial_possibility<=3/16){flag_current_m_state=6;break;}
										if(trial_possibility<=4/16 && trial_possibility>3/16){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>4/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>8/16){flag_current_m_state=0;break;}
										if(trial_possibility<=12/16 && trial_possibility>9/16){
										flag_current_m_state=4;break;}
									}else if(flag_current_m_state==6){
										if(trial_possibility<=1/8){flag_current_m_state=7;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>5/16){flag_current_m_state=1;break;}
										if(trial_possibility<=11/16 && trial_possibility>8/16){flag_current_m_state=5;break;}
									}else if(flag_current_m_state==7){
										if(trial_possibility<=1/8){flag_current_m_state=6;break;}
										if(trial_possibility<=4/8 && trial_possibility>1/8){flag_current_m_state=2;break;}
									}
								}								
							}
							
							int freq_idx[6];
							double intensity_partial[6];
							for(int i=0;i<6;i++){
								freq_idx[i] = lround((nu-nu0+collision_doppler_mhz+freq_offset_mhz[i]-freq_nu_start_mhz)*step_per_mhz);
								if((freq_idx[i]<0)||(freq_idx[i]>=freq_nu_length)){
									intensity_partial[i] = 0;
								}else{
									intensity_partial[i] = int_laser_atom[freq_idx[i]];
								}
							}
							
							double xsect[8];
							int xsect_idx_l_1_start = 2; 
							int xsect_idx_l_2_start = 5; 
							
							// considering saturation intensity, this step calculate the cross section that the laser will hit a sodium atom
							// ground level F=1, M=1, transition_matrix is defined as upper state -> lower state, 
							// calculation is done by Edward, "ESO study", eq 26.
							// transition to lower F=1,M=1 from F'=2, M=2 , upper level 2 to ground state 3
							double xsect_13 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)]));
							//transition from lower F=1,M=1 to F'=2, M=1
							double xsect_23 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)]));
							xsect[2] = xsect_13 + xsect_23;															
							//transition from lower F=1,M=0 to F'=0, M=0							
							double xsect_02 = transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-2]/(1+intensity_partial[xsect_idx_l_1_start-2]*saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)]));
							//transition from lower F=1,M=0 to F'=2, M=0
							double xsect_22 = transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)]));
							xsect[1] = xsect_02 + xsect_22;															
							//transition from lower F=1,M=-1 to F'=1, M=-1							
							double xsect_11 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)]));
							//transition from lower F=1,M=-1 to F'=2, M=-1
							double xsect_21 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)]));
							xsect[0] = xsect_11 + xsect_21;	
														
							//ground level F=2, M=2
							//transition from lower F=2,M=2 to F'=2, M=2							
							double xsect_28 = transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)]));
							//transition from lower F=2,M=2 to F'=3, M=2
							double xsect_38 = transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)]));
							xsect[7] = xsect_28 + xsect_38;															
							
							//ground level F=2, M=1
							//transition from lower F=2,M=1 to F'=1, M=1							
							double xsect_17 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=2, M=1							
							double xsect_27 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=3, M=1
							double xsect_37 = transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)]));
							xsect[6] = xsect_17 + xsect_27 + xsect_37;															
							
							//ground level F=2, M=0
							//transition from lower F=2,M=0 to F'=1, M=0							
							double xsect_16 = transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)]));
							//transition from lower F=2,M=0 to F'=3, M=0
							double xsect_36 = transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)]));
							xsect[5] = xsect_16 + xsect_36;															
							
							//ground level F=2, M=-1
							//transition from lower F=2,M=-1 to F'=1, M=-1							
							double xsect_15 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=2, M=-1							
							double xsect_25 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=3, M=-1
							double xsect_35 = transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)]));
							xsect[4] = xsect_15 + xsect_25 + xsect_35;															

							//ground level F=2, M=-2
							//transition from lower F=2,M=-2 to F'=2, M=-2							
							double xsect_24 = transition_matrix[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)]));
							//transition from lower F=2,M=-2 to F'=3, M=-2
							double xsect_34 = transition_matrix[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)]));
							xsect[3] = xsect_24 + xsect_34;	
							
							ix+=1; 
 							pk = p%n;  
							if(flag_magnetic_field==0){
								pk = 0;  
							}
							// sm indexing is defined as sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j]
							double state_xsect = (xsect[idx_l_1_center-1]*sm[1-1][(0+1)*(2*1+1)+(1-1)][pk]) 
												+(xsect[idx_l_1_center+0]*sm[1-1][(0+1)*(2*1+1)+(1+0)][pk])
												+(xsect[idx_l_1_center+1]*sm[1-1][(0+1)*(2*1+1)+(1+1)][pk]);												
							double prob_xsect = state_xsect * magic_number_backpump;
							double prob_current = gsl_rng_uniform_pos(random_generator);

							if(prob_current<prob_xsect){
								// radiation pressure caused red shift
								nu += 0.05*flag_radiation_pressure; 
								double y = gsl_rng_uniform_pos(random_generator);
								// select which upper level gets chosen
								double yy_78_2 =           sm[1-1][(0+1)*(2*1+1)+(1+1)][pk]*xsect_23/xsect[2];
								double yy_77_2 = yy_78_2 + sm[1-1][(0+1)*(2*1+1)+(0+1)][pk]*xsect_22/xsect[1];
								double yy_76_2 = yy_77_2 + sm[1-1][(0+1)*(2*1+1)+(-1+1)][pk]*xsect_21/xsect[0];
								double yy_78_1 = yy_76_2 + sm[1-1][(0+1)*(2*1+1)+(1+1)][pk]*xsect_13/xsect[2];
								double yy_76_1 = yy_78_1 + sm[1-1][(0+1)*(2*1+1)+(-1+1)][pk]*xsect_11/xsect[0];
								double yy_77_0 = yy_76_1 + sm[1-1][(0+1)*(2*1+1)+(0+1)][pk]*xsect_02/xsect[1];
								
								if(y<yy_78_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<15){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<20){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_77_2){
									//transit from F=2,M=2, to F=2,M=1
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<30){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;	
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;	
									}else if(yr<55){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;	
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_76_2){
									//transit from F=2,M=2, to F=2, M=0
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;	
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75; 
									}
									break;
								}else if(y<yy_78_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<1){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<4){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_76_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<6){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<9){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1; 
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_77_0){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<40){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}
							}else{
								p += 1;
							}					
						}
					break;}
					case 2:{
						// F=1 M=1
						while(p<kp){
							double time_current_s = ix*time_delta_s;  
							if(ix>=steps || time_current_s>time_readin[data_length-1]){
								break; 
							}
							double intensity_current_w_m2 = gsl_spline_eval(pulse_data_spline, time_current_s, pulse_data_acc); 
							// define total intensity at mesosphere in watts/m^2
							double intensity = intensity_current_w_m2*intensity_normalized_factor;  
							double intensity_backpump = intensity*alpha;

							// define conversion rate for power/arcsec <- photon_number/sec
							double magic_number_backpump = 8192.33 * time_delta_s * intensity_backpump;
							
							// calculate saturation parameters, laster intensity/saturation intensity
							for(int i=0;i<rows*columns;i++){
								if(saturation_intensity[i]!=0){
									saturation_parameter[i] = intensity/saturation_intensity[i];
								}
							}
							
							saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							
							velocity_sodium_pre = velocity_sodium_after;
							double collision_probability = 2*K*temperature*ns*thetas/sqrt(M_PI)/mass_nitrogen/velocity_sodium_pre*psi(velocity_sodium_pre*sqrt(mass_nitrogen/(2*K*temperature)))*time_delta_s;							
							bool collided = (collision_probability>gsl_rng_uniform_pos(random_generator));	
							double collision_doppler_mhz = 0;
							if(collided && flag_spin_exchange == 1){
								particle_collision_count += 1;
								particle_collision_count  = particle_collision_count % 10;

								// velocity of nitrogen atom, angle angel_sodium_nitrogen is between Na atom velocity and N2 atom velocity
								double velocity_nitrogen = 415*pow((-log(gsl_rng_uniform_pos(random_generator))),0.4);
								double angel_sodium_nitrogen = acos(2*(gsl_rng_uniform_pos(random_generator)-0.5));
								// relative velocity between two particles
								double velocity_relative_sodium_nitrogen = sqrt(pow(velocity_sodium_pre,2)+pow(velocity_nitrogen,2)-2*velocity_sodium_pre*velocity_nitrogen*cos(angel_sodium_nitrogen));
								double angel_relative_sodium_nitrogen = pow(velocity_nitrogen,2.)>=(pow(velocity_sodium_pre,2.)+pow(velocity_relative_sodium_nitrogen,2.))? M_PI-asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen):asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen);//calculate relative velocity direction respect to sodium atom
								
								// hard sphere interaction in CM reference frame
								double xx_hs = pow(gsl_rng_uniform_pos(random_generator),0.5);
								double thetacm_hs = 2*acos(xx_hs);
								double dvnacm_hs = (relative_mass/(1+relative_mass))*velocity_relative_sodium_nitrogen;
								double dvna_hs = 2*dvnacm_hs*sin(thetacm_hs/2);
								double theta_hs = atan(sin(thetacm_hs)/(1-cos(thetacm_hs)));
								double psi_hs = 2*M_PI*gsl_rng_uniform_pos(random_generator);
								double dvna1_hs = dvna_hs*pow(pow(cos(theta_hs),2)+pow(sin(theta_hs)*cos(psi_hs),2),0.5);
								double theta1_hs = atan(sin(theta_hs)*cos(psi_hs)/cos(theta_hs));
								double phi_hs = angel_relative_sodium_nitrogen + theta1_hs;
								
								velocity_sodium_after = sqrt((pow(velocity_sodium_pre,2.0)+pow(dvna1_hs,2.0)-2*velocity_sodium_pre*dvna1_hs*cos(phi_hs))+pow(dvna_hs*sin(theta_hs)*sin(psi_hs),2.0));
								double beta1 = 2*M_PI*gsl_rng_uniform_pos(random_generator)+atan(dvna_hs*sin(theta_hs)*sin(psi_hs)/dvna1_hs);
								double dalfa =  acos((pow(velocity_sodium_after,2.)+pow(velocity_sodium_pre,2.0)-pow(dvna_hs,2.))/(2*velocity_sodium_after*velocity_sodium_pre));
								angle_sodium_laser = acos(cos(angle_sodium_laser)*cos(dalfa)+sin(angle_sodium_laser)*sin(dalfa)*cos(M_PI-beta1));
								collision_doppler_mhz = velocity_sodium_after*cos(angle_sodium_laser)*nu_na/(2.998*1E8)/1e6;
								if(particle_collision_count==0&&flag_oxygen_spin_exchange){
									// this is going to hit with an oxygen particle.
									// 1 in 5 in air is oxygen, spin exchange by 1/2, so 1 in 10s collision will be for oxygen
									double trial_possibility = gsl_rng_uniform_pos(random_generator);	
									if(flag_current_m_state==0){
										if(trial_possibility<=1/16){flag_current_m_state=1;break;}
										if(trial_possibility<=1/8 && trial_possibility>1/16){flag_current_m_state=5;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=3;break;}
										// or it may just stay in this state
									}else if(flag_current_m_state==1){
										if(trial_possibility<=1/16){flag_current_m_state=2;break;}
										if(trial_possibility<=1/4 && trial_possibility>1/16){flag_current_m_state=6;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/4){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=4;break;}
										if(trial_possibility<=3/4 && trial_possibility>11/16){flag_current_m_state = 0;break;}
									}else if(flag_current_m_state==2){
										if(trial_possibility<=3/8){flag_current_m_state=7;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=6;break;}
										if(trial_possibility<=10/16 && trial_possibility>9/16){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=1;break;}
									}else if(flag_current_m_state==3){
										if(trial_possibility<=1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/8){flag_current_m_state=0;break;}
									}else if(flag_current_m_state==4){
										if(trial_possibility<=3/16){flag_current_m_state=5;break;}
										if(trial_possibility<=3/8 && trial_possibility>3/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=0;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=3;break;}
									}else if(flag_current_m_state==5){
										if(trial_possibility<=3/16){flag_current_m_state=6;break;}
										if(trial_possibility<=4/16 && trial_possibility>3/16){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>4/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>8/16){flag_current_m_state=0;break;}
										if(trial_possibility<=12/16 && trial_possibility>9/16){
										flag_current_m_state=4;break;}
									}else if(flag_current_m_state==6){
										if(trial_possibility<=1/8){flag_current_m_state=7;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>5/16){flag_current_m_state=1;break;}
										if(trial_possibility<=11/16 && trial_possibility>8/16){flag_current_m_state=5;break;}
									}else if(flag_current_m_state==7){
										if(trial_possibility<=1/8){flag_current_m_state=6;break;}
										if(trial_possibility<=4/8 && trial_possibility>1/8){flag_current_m_state=2;break;}
									}
								}								
							}
											
							int freq_idx[6];
							double intensity_partial[6];
							for(int i=0;i<6;i++){
								freq_idx[i] = lround((nu-nu0+collision_doppler_mhz+freq_offset_mhz[i]-freq_nu_start_mhz)*step_per_mhz);
								if((freq_idx[i]<0)||(freq_idx[i]>=freq_nu_length)){
									intensity_partial[i] = 0;
								}else{
									intensity_partial[i] = int_laser_atom[freq_idx[i]];
								}
							}
							
							double xsect[8];
							// start index for F=1,M=1 lower state, relating to the intensity_partial definition
							int xsect_idx_l_1_start = 2; 
							// start index for F=2,M=2 lower state, relating to the intensity_partial definition
							int xsect_idx_l_2_start = 5; 
							
							// considering saturation intensity, this step calculate the cross section that the laser will hit a sodium atom
							// ground level F=1, M=1, transition_matrix is defined as upper state -> lower state, 
							// calculation is done by Edward, "ESO study", eq 26.
							// transition to lower F=1,M=1 from F'=2, M=2 , upper level 2 to ground state 3
							double xsect_13 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)]));
							// transition from lower F=1,M=1 to F'=2, M=1
							double xsect_23 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)]));
							xsect[2] = xsect_13 + xsect_23;															
							// transition from lower F=1,M=0 to F'=0, M=0							
							double xsect_02 = transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-2]/(1+intensity_partial[xsect_idx_l_1_start-2]*saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)]));
							// transition from lower F=1,M=0 to F'=2, M=0
							double xsect_22 = transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)]));
							xsect[1] = xsect_02 + xsect_22;															
							// transition from lower F=1,M=-1 to F'=1, M=-1							
							double xsect_11 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)]));
							// transition from lower F=1,M=-1 to F'=2, M=-1
							double xsect_21 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)]));
							xsect[0] = xsect_11 + xsect_21;	
														
							// ground level F=2, M=2
							// transition from lower F=2,M=2 to F'=2, M=2							
							double xsect_28 = transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)]));
							// transition from lower F=2,M=2 to F'=3, M=2
							double xsect_38 = transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)]));
							xsect[7] = xsect_28 + xsect_38;															
							
							// ground level F=2, M=1
							// transition from lower F=2,M=1 to F'=1, M=1							
							double xsect_17 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)]));
							// transition from lower F=2,M=1 to F'=2, M=1							
							double xsect_27 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)]));
							// transition from lower F=2,M=1 to F'=3, M=1
							double xsect_37 = transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)]));
							xsect[6] = xsect_17 + xsect_27 + xsect_37;															
							
							// ground level F=2, M=0
							// transition from lower F=2,M=0 to F'=1, M=0							
							double xsect_16 = transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)]));
							// transition from lower F=2,M=0 to F'=3, M=0
							double xsect_36 = transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)]));
							xsect[5] = xsect_16 + xsect_36;															
							
							// ground level F=2, M=-1
							// transition from lower F=2,M=-1 to F'=1, M=-1							
							double xsect_15 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)]));
							// transition from lower F=2,M=-1 to F'=2, M=-1							
							double xsect_25 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)]));
							// transition from lower F=2,M=-1 to F'=3, M=-1
							double xsect_35 = transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)]));
							xsect[4] = xsect_15 + xsect_25 + xsect_35;															

							// ground level F=2, M=-2
							// transition from lower F=2,M=-2 to F'=2, M=-2							
							double xsect_24 = transition_matrix[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)]));
							//transition from lower F=2,M=-2 to F'=3, M=-2
							double xsect_34 = transition_matrix[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)]));
							xsect[3] = xsect_24 + xsect_34;	
							
							// total time increment
							ix+=1;
							// n is floor(larmor_precession_time/delta_time)
							pk = p%n; 
							if(flag_magnetic_field==0){
								pk = 0; 
							}
							// sm indexing is defined as sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j]
							double state_xsect = (xsect[idx_l_1_center-1]*sm[1-1][(1+1)*(2*1+1)+(1-1)][pk]) 
									+(xsect[idx_l_1_center+0]*sm[1-1][(1+1)*(2*1+1)+(1+0)][pk])
									+(xsect[idx_l_1_center+1]*sm[1-1][(1+1)*(2*1+1)+(1+1)][pk]);												
							double prob_xsect = state_xsect * magic_number_backpump;
							double prob_current = gsl_rng_uniform_pos(random_generator);
							
							if(prob_current<prob_xsect){
								// radiation pressure caused red shift
								nu += 0.05*flag_radiation_pressure; 
								double y = gsl_rng_uniform_pos(random_generator);
								// select which upper level gets chosen
								double yy_88_2 =           sm[1-1][(1+1)*(2*1+1)+(1+1)][pk]*xsect_23/xsect[2];
								double yy_87_2 = yy_88_2 + sm[1-1][(1+1)*(2*1+1)+(0+1)][pk]*xsect_22/xsect[1];
								double yy_86_2 = yy_87_2 + sm[1-1][(1+1)*(2*1+1)+(-1+1)][pk]*xsect_21/xsect[0];
								double yy_88_1 = yy_86_2 + sm[1-1][(1+1)*(2*1+1)+(1+1)][pk]*xsect_13/xsect[2];
								double yy_86_1 = yy_88_1 + sm[1-1][(1+1)*(2*1+1)+(-1+1)][pk]*xsect_11/xsect[0];
								double yy_87_0 = yy_86_1 + sm[1-1][(1+1)*(2*1+1)+(0+1)][pk]*xsect_02/xsect[1];
								
								
								if(y<yy_88_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<15){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<20){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_87_2){
									//transit from F=2,M=2, to F=2,M=1
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<30){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;	
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;	
									}else if(yr<55){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;	
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_86_2){
									//transit from F=2,M=2, to F=2, M=0
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;		
									}else if(yr<45){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;	
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;  
									}
									break;
								}else if(y<yy_88_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<1){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<4){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_86_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<6){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<9){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1; 
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_87_0){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<40){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}
							}else{
								p += 1;
							}					
						}
					break;}
					case 3:{
					// F=2 M=-2
						while(p<kp){
							double time_current_s = ix*time_delta_s;
							if(ix>=steps || time_current_s>time_readin[data_length-1]){
								break; 
							}
							double intensity_current_w_m2 = gsl_spline_eval(pulse_data_spline, time_current_s, pulse_data_acc);
							// define total intensity at mesosphere in watts/m^2
							double intensity = intensity_current_w_m2*intensity_normalized_factor; 

							// define conversion rate for power/arcsec <- photon_number/sec
							double magic_number = 8192.33 * time_delta_s * intensity;
							
							// calculate saturation parameters, laser intensity/saturation intensity
							for(int i=0;i<rows*columns;i++){
								if(saturation_intensity[i]!=0){
									saturation_parameter[i] = intensity/saturation_intensity[i];
								}
							}
							saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *= alpha;

							velocity_sodium_pre = velocity_sodium_after;
							double collision_probability = 2*K*temperature*ns*thetas/sqrt(M_PI)/mass_nitrogen/velocity_sodium_pre*psi(velocity_sodium_pre*sqrt(mass_nitrogen/(2*K*temperature)))*time_delta_s;
							bool collided = (collision_probability>gsl_rng_uniform_pos(random_generator));	
							double collision_doppler_mhz = 0;
							if(collided && flag_spin_exchange == 1){
								particle_collision_count += 1;
								particle_collision_count  = particle_collision_count % 10;

								// velocity of nitrogen atom, angle angel_sodium_nitrogen is between Na atom velocity and N2 atom velocity
								double velocity_nitrogen = 415*pow((-log(gsl_rng_uniform_pos(random_generator))),0.4);
								double angel_sodium_nitrogen = acos(2*(gsl_rng_uniform_pos(random_generator)-0.5));
								// relative velocity between two particles
								double velocity_relative_sodium_nitrogen = sqrt(pow(velocity_sodium_pre,2)+pow(velocity_nitrogen,2)-2*velocity_sodium_pre*velocity_nitrogen*cos(angel_sodium_nitrogen));
								double angel_relative_sodium_nitrogen = pow(velocity_nitrogen,2.)>=(pow(velocity_sodium_pre,2.)+pow(velocity_relative_sodium_nitrogen,2.))? M_PI-asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen):asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen);//calculate relative velocity direction respect to sodium atom
								
								// hard sphere interaction in CM reference frame
								double xx_hs = pow(gsl_rng_uniform_pos(random_generator),0.5);
								double thetacm_hs = 2*acos(xx_hs);
								double dvnacm_hs = (relative_mass/(1+relative_mass))*velocity_relative_sodium_nitrogen;
								double dvna_hs = 2*dvnacm_hs*sin(thetacm_hs/2);
								double theta_hs = atan(sin(thetacm_hs)/(1-cos(thetacm_hs)));
								double psi_hs = 2*M_PI*gsl_rng_uniform_pos(random_generator);
								double dvna1_hs = dvna_hs*pow(pow(cos(theta_hs),2)+pow(sin(theta_hs)*cos(psi_hs),2),0.5);
								double theta1_hs = atan(sin(theta_hs)*cos(psi_hs)/cos(theta_hs));
								double phi_hs = angel_relative_sodium_nitrogen + theta1_hs;
								
								velocity_sodium_after = sqrt((pow(velocity_sodium_pre,2.0)+pow(dvna1_hs,2.0)-2*velocity_sodium_pre*dvna1_hs*cos(phi_hs))+pow(dvna_hs*sin(theta_hs)*sin(psi_hs),2.0));
								double beta1 = 2*M_PI*gsl_rng_uniform_pos(random_generator)+atan(dvna_hs*sin(theta_hs)*sin(psi_hs)/dvna1_hs);
								double dalfa =  acos((pow(velocity_sodium_after,2.)+pow(velocity_sodium_pre,2.0)-pow(dvna_hs,2.))/(2*velocity_sodium_after*velocity_sodium_pre));
								angle_sodium_laser = acos(cos(angle_sodium_laser)*cos(dalfa)+sin(angle_sodium_laser)*sin(dalfa)*cos(M_PI-beta1));
								collision_doppler_mhz = velocity_sodium_after*cos(angle_sodium_laser)*nu_na/(2.998*1E8)/1e6;
								if(particle_collision_count==0&&flag_oxygen_spin_exchange){
									// this is going to hit with an oxygen particle.
									// 1 in 5 in air is oxygen, spin exchange by 1/2, so 1 in 10s collision will be for oxygen
									double trial_possibility = gsl_rng_uniform_pos(random_generator);	
									if(flag_current_m_state==0){
										if(trial_possibility<=1/16){flag_current_m_state=1;break;}
										if(trial_possibility<=1/8 && trial_possibility>1/16){flag_current_m_state=5;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=3;break;}
										// or it may just stay in this state
									}else if(flag_current_m_state==1){
										if(trial_possibility<=1/16){flag_current_m_state=2;break;}
										if(trial_possibility<=1/4 && trial_possibility>1/16){flag_current_m_state=6;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/4){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=4;break;}
										if(trial_possibility<=3/4 && trial_possibility>11/16){flag_current_m_state = 0;break;}
									}else if(flag_current_m_state==2){
										if(trial_possibility<=3/8){flag_current_m_state=7;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=6;break;}
										if(trial_possibility<=10/16 && trial_possibility>9/16){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=1;break;}
									}else if(flag_current_m_state==3){
										if(trial_possibility<=1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/8){flag_current_m_state=0;break;}
									}else if(flag_current_m_state==4){
										if(trial_possibility<=3/16){flag_current_m_state=5;break;}
										if(trial_possibility<=3/8 && trial_possibility>3/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=0;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=3;break;}
									}else if(flag_current_m_state==5){
										if(trial_possibility<=3/16){flag_current_m_state=6;break;}
										if(trial_possibility<=4/16 && trial_possibility>3/16){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>4/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>8/16){flag_current_m_state=0;break;}
										if(trial_possibility<=12/16 && trial_possibility>9/16){
										flag_current_m_state=4;break;}
									}else if(flag_current_m_state==6){
										if(trial_possibility<=1/8){flag_current_m_state=7;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>5/16){flag_current_m_state=1;break;}
										if(trial_possibility<=11/16 && trial_possibility>8/16){flag_current_m_state=5;break;}
									}else if(flag_current_m_state==7){
										if(trial_possibility<=1/8){flag_current_m_state=6;break;}
										if(trial_possibility<=4/8 && trial_possibility>1/8){flag_current_m_state=2;break;}
									}
								}								
							}
							
				
							int freq_idx[6];
							double intensity_partial[6];
							for(int i=0;i<6;i++){
								freq_idx[i] = lround((nu-nu0+collision_doppler_mhz+freq_offset_mhz[i]-freq_nu_start_mhz)*step_per_mhz);
								if((freq_idx[i]<0)||(freq_idx[i]>=freq_nu_length)){
									intensity_partial[i] = 0;
								}else{
									intensity_partial[i] = int_laser_atom[freq_idx[i]];
								}
							}
							
							double xsect[8];
							int xsect_idx_l_1_start = 2; 
							int xsect_idx_l_2_start = 5; 
							
							// considering saturation intensity, this step calculate the cross section that the laser will hit a sodium atom
							// ground level F=1, M=1, transition_matrix is defined as upper state -> lower state, 
							// calculation is done by Edward, "ESO study", eq 26.
							//transition to lower F=1,M=1 from F'=2, M=2 , upper level 2 to ground state 3
							double xsect_13 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)]));
							//transition from lower F=1,M=1 to F'=2, M=1
							double xsect_23 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)]));
							xsect[2] = xsect_13 + xsect_23;															
							//transition from lower F=1,M=0 to F'=0, M=0							
							double xsect_02 = transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-2]/(1+intensity_partial[xsect_idx_l_1_start-2]*saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)]));
							//transition from lower F=1,M=0 to F'=2, M=0
							double xsect_22 = transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)]));
							xsect[1] = xsect_02 + xsect_22;															
							//transition from lower F=1,M=-1 to F'=1, M=-1							
							double xsect_11 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)]));
							//transition from lower F=1,M=-1 to F'=2, M=-1
							double xsect_21 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)]));
							xsect[0] = xsect_11 + xsect_21;	
														
							//ground level F=2, M=2
							//transition from lower F=2,M=2 to F'=2, M=2							
							double xsect_28 = transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)]));
							//transition from lower F=2,M=2 to F'=3, M=2
							double xsect_38 = transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)]));
							xsect[7] = xsect_28 + xsect_38;															
							
							//ground level F=2, M=1
							//transition from lower F=2,M=1 to F'=1, M=1							
							double xsect_17 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=2, M=1							
							double xsect_27 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=3, M=1
							double xsect_37 = transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)]));
							xsect[6] = xsect_17 + xsect_27 + xsect_37;															
							
							//ground level F=2, M=0
							//transition from lower F=2,M=0 to F'=1, M=0							
							double xsect_16 = transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)]));
							//transition from lower F=2,M=0 to F'=3, M=0
							double xsect_36 = transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)]));
							xsect[5] = xsect_16 + xsect_36;															
							
							//ground level F=2, M=-1
							//transition from lower F=2,M=-1 to F'=1, M=-1							
							double xsect_15 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=2, M=-1							
							double xsect_25 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=3, M=-1
							double xsect_35 = transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)]));
							xsect[4] = xsect_15 + xsect_25 + xsect_35;															

							//ground level F=2, M=-2
							//transition from lower F=2,M=-2 to F'=2, M=-2							
							double xsect_24 = transition_matrix[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)]));
							//transition from lower F=2,M=-2 to F'=3, M=-2
							double xsect_34 = transition_matrix[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)]));
							xsect[3] = xsect_24 + xsect_34;	
							
							// total time increment
							ix+=1; 
							// n is floor(larmor_precession_time/delta_time)
							pk = p%n; 
							if(flag_magnetic_field==0){
								pk = 0;  
							}
							// sm indexing is defined as sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j]
							double state_xsect = (xsect[idx_l_2_center-2]*sm[2-1][(-2+2)*(2*2+1)+(2-2)][pk]) 
									    +(xsect[idx_l_2_center-1]*sm[2-1][(-2+2)*(2*2+1)+(2-1)][pk])
									    +(xsect[idx_l_2_center+0]*sm[2-1][(-2+2)*(2*2+1)+(2+0)][pk])
									    +(xsect[idx_l_2_center+1]*sm[2-1][(-2+2)*(2*2+1)+(2+1)][pk])
									    +(xsect[idx_l_2_center+2]*sm[2-1][(-2+2)*(2*2+1)+(2+2)][pk]);
							double prob_xsect = state_xsect * magic_number;
							double prob_current = gsl_rng_uniform_pos(random_generator);
							
							if(prob_current<prob_xsect){
								// radiation pressure caused red shift
								nu += 0.05*flag_radiation_pressure; 
								double y = gsl_rng_uniform_pos(random_generator);
								// select which upper level gets chosen
								double yy_15_3 =           sm[2-1][(-2+2)*(2*2+1)+(2+2)][pk]*xsect_38/xsect[7];
								double yy_14_3 = yy_15_3 + sm[2-1][(-2+2)*(2*2+1)+(1+2)][pk]*xsect_37/xsect[6];
								double yy_13_3 = yy_14_3 + sm[2-1][(-2+2)*(2*2+1)+(0+2)][pk]*xsect_36/xsect[5];
								double yy_12_3 = yy_13_3 + sm[2-1][(-2+2)*(2*2+1)+(-1+2)][pk]*xsect_35/xsect[4];
								double yy_11_3 = yy_12_3 + sm[2-1][(-2+2)*(2*2+1)+(-2+2)][pk]*xsect_34/xsect[3];
								double yy_15_2 = yy_11_3 + sm[2-1][(-2+2)*(2*2+1)+(2+2)][pk]*xsect_28/xsect[7];
								double yy_14_2 = yy_15_2 + sm[2-1][(-2+2)*(2*2+1)+(1+2)][pk]*xsect_27/xsect[6];
								double yy_12_2 = yy_14_2 + sm[2-1][(-2+2)*(2*2+1)+(-1+2)][pk]*xsect_25/xsect[4];
								double yy_11_2 = yy_12_2 + sm[2-1][(-2+2)*(2*2+1)+(-2+2)][pk]*xsect_24/xsect[3];
								double yy_14_1 = yy_11_2 + sm[2-1][(-2+2)*(2*2+1)+(1+2)][pk]*xsect_17/xsect[6];
								double yy_13_1 = yy_14_1 + sm[2-1][(-2+2)*(2*2+1)+(0+2)][pk]*xsect_16/xsect[5];
								double yy_12_1 = yy_13_1 + sm[2-1][(-2+2)*(2*2+1)+(-1+2)][pk]*xsect_15/xsect[4];
								
								if(y<yy_15_3){
									//stays at F=2,M=2;
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<40){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_14_3){
									//transit from F=2,M=2, to F=2,M=1
									double yr = gsl_rng_uniform_pos(random_generator)*60; 
									if(yr<24){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<56){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_13_3){
									//transit from F=2,M=2, to F=2, M=0
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<12){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<48){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5; 
									}else{
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;  
									}
									break;
								}else if(y<yy_12_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<4){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<36){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_11_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 4; 
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_15_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<30){
										flag_current_m_state = 7; // diff here
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_14_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<15){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<20){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_12_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<15){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_11_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_14_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<1){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<4){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_13_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<3){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<7){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_12_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<6){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<9){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}
							}else{
								p += 1;
							}
						}
					break;}
					case 4:{
						while(p<kp){
							// simulation step time
							double time_current_s = ix*time_delta_s; 
							if(ix>=steps || time_current_s>time_readin[data_length-1]){
								break; 
							}
							// intensity at the simulation time
							double intensity_current_w_m2 = gsl_spline_eval(pulse_data_spline, time_current_s, pulse_data_acc);

							// define total intensity at mesosphere in watts/m^2
							double intensity = intensity_current_w_m2*intensity_normalized_factor; //10W/m^2, change it to suit applications

							// define conversion rate for power/arcsec <- photon_number/sec
							double magic_number = 8192.33 * time_delta_s * intensity;
							
							// calculate saturation parameters, laster intensity/saturation intensity
							for(int i=0;i<rows*columns;i++){
								if(saturation_intensity[i]!=0){
									saturation_parameter[i] = intensity/saturation_intensity[i];
								}
							}
							
							saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *= alpha;
								
							velocity_sodium_pre = velocity_sodium_after;
							double collision_probability = 2*K*temperature*ns*thetas/sqrt(M_PI)/mass_nitrogen/velocity_sodium_pre*psi(velocity_sodium_pre*sqrt(mass_nitrogen/(2*K*temperature)))*time_delta_s;
							bool collided = (collision_probability>gsl_rng_uniform_pos(random_generator));	
							double collision_doppler_mhz = 0;
							if(collided && flag_spin_exchange == 1){
								particle_collision_count += 1;
								particle_collision_count  = particle_collision_count % 10;
								// velocity of nitrogen atom, angle angel_sodium_nitrogen is between Na atom velocity and N2 atom velocity
								double velocity_nitrogen = 415*pow((-log(gsl_rng_uniform_pos(random_generator))),0.4);
								double angel_sodium_nitrogen = acos(2*(gsl_rng_uniform_pos(random_generator)-0.5));
								// relative velocity between two particles
								double velocity_relative_sodium_nitrogen = sqrt(pow(velocity_sodium_pre,2)+pow(velocity_nitrogen,2)-2*velocity_sodium_pre*velocity_nitrogen*cos(angel_sodium_nitrogen));
								double angel_relative_sodium_nitrogen = pow(velocity_nitrogen,2.)>=(pow(velocity_sodium_pre,2.)+pow(velocity_relative_sodium_nitrogen,2.))? M_PI-asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen):asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen);//calculate relative velocity direction respect to sodium atom
								
								// hard sphere interaction in CM reference frame
								double xx_hs = pow(gsl_rng_uniform_pos(random_generator),0.5);
								double thetacm_hs = 2*acos(xx_hs);
								double dvnacm_hs = (relative_mass/(1+relative_mass))*velocity_relative_sodium_nitrogen;
								double dvna_hs = 2*dvnacm_hs*sin(thetacm_hs/2);
								double theta_hs = atan(sin(thetacm_hs)/(1-cos(thetacm_hs)));
								double psi_hs = 2*M_PI*gsl_rng_uniform_pos(random_generator);
								double dvna1_hs = dvna_hs*pow(pow(cos(theta_hs),2)+pow(sin(theta_hs)*cos(psi_hs),2),0.5);
								double theta1_hs = atan(sin(theta_hs)*cos(psi_hs)/cos(theta_hs));
								double phi_hs = angel_relative_sodium_nitrogen + theta1_hs;
								
								velocity_sodium_after = sqrt((pow(velocity_sodium_pre,2.0)+pow(dvna1_hs,2.0)-2*velocity_sodium_pre*dvna1_hs*cos(phi_hs))+pow(dvna_hs*sin(theta_hs)*sin(psi_hs),2.0));
								double beta1 = 2*M_PI*gsl_rng_uniform_pos(random_generator)+atan(dvna_hs*sin(theta_hs)*sin(psi_hs)/dvna1_hs);
								double dalfa =  acos((pow(velocity_sodium_after,2.)+pow(velocity_sodium_pre,2.0)-pow(dvna_hs,2.))/(2*velocity_sodium_after*velocity_sodium_pre));
								angle_sodium_laser = acos(cos(angle_sodium_laser)*cos(dalfa)+sin(angle_sodium_laser)*sin(dalfa)*cos(M_PI-beta1));
								collision_doppler_mhz = velocity_sodium_after*cos(angle_sodium_laser)*nu_na/(2.998*1E8)/1e6;
								if(particle_collision_count==0&&flag_oxygen_spin_exchange){
									// this is going to hit with an oxygen particle.
									// 1 in 5 in air is oxygen, spin exchange by 1/2, so 1 in 10s collision will be for oxygen
									double trial_possibility = gsl_rng_uniform_pos(random_generator);	
									if(flag_current_m_state==0){
										if(trial_possibility<=1/16){flag_current_m_state=1;break;}
										if(trial_possibility<=1/8 && trial_possibility>1/16){flag_current_m_state=5;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=3;break;}
										// or it may just stay in this state
									}else if(flag_current_m_state==1){
										if(trial_possibility<=1/16){flag_current_m_state=2;break;}
										if(trial_possibility<=1/4 && trial_possibility>1/16){flag_current_m_state=6;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/4){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=4;break;}
										if(trial_possibility<=3/4 && trial_possibility>11/16){flag_current_m_state = 0;break;}
									}else if(flag_current_m_state==2){
										if(trial_possibility<=3/8){flag_current_m_state=7;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=6;break;}
										if(trial_possibility<=10/16 && trial_possibility>9/16){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=1;break;}
									}else if(flag_current_m_state==3){
										if(trial_possibility<=1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/8){flag_current_m_state=0;break;}
									}else if(flag_current_m_state==4){
										if(trial_possibility<=3/16){flag_current_m_state=5;break;}
										if(trial_possibility<=3/8 && trial_possibility>3/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=0;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=3;break;}
									}else if(flag_current_m_state==5){
										if(trial_possibility<=3/16){flag_current_m_state=6;break;}
										if(trial_possibility<=4/16 && trial_possibility>3/16){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>4/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>8/16){flag_current_m_state=0;break;}
										if(trial_possibility<=12/16 && trial_possibility>9/16){
										flag_current_m_state=4;break;}
									}else if(flag_current_m_state==6){
										if(trial_possibility<=1/8){flag_current_m_state=7;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>5/16){flag_current_m_state=1;break;}
										if(trial_possibility<=11/16 && trial_possibility>8/16){flag_current_m_state=5;break;}
									}else if(flag_current_m_state==7){
										if(trial_possibility<=1/8){flag_current_m_state=6;break;}
										if(trial_possibility<=4/8 && trial_possibility>1/8){flag_current_m_state=2;break;}
									}
								}								
							}
											
							int freq_idx[6];
							double intensity_partial[6];
							for(int i=0;i<6;i++){
								freq_idx[i] = lround((nu-nu0+collision_doppler_mhz+freq_offset_mhz[i]-freq_nu_start_mhz)*step_per_mhz);
								if((freq_idx[i]<0)||(freq_idx[i]>=freq_nu_length)){
									intensity_partial[i] = 0;
								}else{
									intensity_partial[i] = int_laser_atom[freq_idx[i]];
								}
							}
							
							double xsect[8];
							// start index for F=1,M=1 lower state, relating to the intensity_partial definition 
							int xsect_idx_l_1_start = 2; 
							// start index for F=2,M=2 lower state, relating to the intensity_partial definition
							int xsect_idx_l_2_start = 5; 
							
							// considering saturation intensity, this step calculate the cross section that the laser will hit a sodium atom
							// ground level F=1, M=1, transition_matrix is defined as upper state -> lower state, 
							// calculation is done by Edward, "ESO study", eq 26.
							// transition to lower F=1,M=1 from F'=2, M=2 , upper level 2 to ground state 3
							//transition to lower F=1,M=1 from F'=2, M=2 , upper level 2 to ground state 3
							double xsect_13 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)]));
							//transition from lower F=1,M=1 to F'=2, M=1
							double xsect_23 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)]));
							xsect[2] = xsect_13 + xsect_23;															
							//transition from lower F=1,M=0 to F'=0, M=0							
							double xsect_02 = transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-2]/(1+intensity_partial[xsect_idx_l_1_start-2]*saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)]));
							//transition from lower F=1,M=0 to F'=2, M=0
							double xsect_22 = transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)]));
							xsect[1] = xsect_02 + xsect_22;															
							//transition from lower F=1,M=-1 to F'=1, M=-1							
							double xsect_11 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)]));
							//transition from lower F=1,M=-1 to F'=2, M=-1
							double xsect_21 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)]));
							xsect[0] = xsect_11 + xsect_21;	
														
							//ground level F=2, M=2
							//transition from lower F=2,M=2 to F'=2, M=2							
							double xsect_28 = transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)]));
							//transition from lower F=2,M=2 to F'=3, M=2
							double xsect_38 = transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)]));
							xsect[7] = xsect_28 + xsect_38;															
							
							//ground level F=2, M=1
							//transition from lower F=2,M=1 to F'=1, M=1							
							double xsect_17 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=2, M=1							
							double xsect_27 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=3, M=1
							double xsect_37 = transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)]));
							xsect[6] = xsect_17 + xsect_27 + xsect_37;															
							
							//ground level F=2, M=0
							//transition from lower F=2,M=0 to F'=1, M=0							
							double xsect_16 = transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)]));
							//transition from lower F=2,M=0 to F'=3, M=0
							double xsect_36 = transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)]));
							xsect[5] = xsect_16 + xsect_36;															
							
							//ground level F=2, M=-1
							//transition from lower F=2,M=-1 to F'=1, M=-1							
							double xsect_15 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=2, M=-1							
							double xsect_25 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=3, M=-1
							double xsect_35 = transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)]));
							xsect[4] = xsect_15 + xsect_25 + xsect_35;															

							//ground level F=2, M=-2
							//transition from lower F=2,M=-2 to F'=2, M=-2							
							double xsect_24 = transition_matrix[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)]));
							//transition from lower F=2,M=-2 to F'=3, M=-2
							double xsect_34 = transition_matrix[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)]));
							xsect[3] = xsect_24 + xsect_34;	
						
							// total time increment
							ix+=1; 
							// n is floor(larmor_precession_time/delta_time)
							pk = p%n; 
							if(flag_magnetic_field==0){
								// do not need to consider about magnetic level change 
								pk = 0; 
							}
							// sm indexing is defined as sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j]
							double state_xsect = (xsect[idx_l_2_center-2]*sm[2-1][(-1+2)*(2*2+1)+(2-2)][pk]) 
												+(xsect[idx_l_2_center-1]*sm[2-1][(-1+2)*(2*2+1)+(2-1)][pk])
												+(xsect[idx_l_2_center+0]*sm[2-1][(-1+2)*(2*2+1)+(2+0)][pk])
												+(xsect[idx_l_2_center+1]*sm[2-1][(-1+2)*(2*2+1)+(2+1)][pk])
												+(xsect[idx_l_2_center+2]*sm[2-1][(-1+2)*(2*2+1)+(2+2)][pk]);
							double prob_xsect = state_xsect * magic_number;
							double prob_current = gsl_rng_uniform_pos(random_generator);
							if(prob_current<prob_xsect){
								// radiation pressure caused red shift
								nu += 0.05*flag_radiation_pressure; 
								double y = gsl_rng_uniform_pos(random_generator);
								// select which upper level gets chosen
								double yy_25_3 = 		   sm[2-1][(-1+2)*(2*2+1)+(2+2)][pk]*xsect_38/xsect[7];
								double yy_24_3 = yy_25_3 + sm[2-1][(-1+2)*(2*2+1)+(1+2)][pk]*xsect_37/xsect[6];
								double yy_23_3 = yy_24_3 + sm[2-1][(-1+2)*(2*2+1)+(0+2)][pk]*xsect_36/xsect[5];
								double yy_22_3 = yy_23_3 + sm[2-1][(-1+2)*(2*2+1)+(-1+2)][pk]*xsect_35/xsect[4];
								double yy_21_3 = yy_22_3 + sm[2-1][(-1+2)*(2*2+1)+(-2+2)][pk]*xsect_34/xsect[3];
								double yy_25_2 = yy_21_3 + sm[2-1][(-1+2)*(2*2+1)+(2+2)][pk]*xsect_28/xsect[7];
								double yy_24_2 = yy_25_2 + sm[2-1][(-1+2)*(2*2+1)+(1+2)][pk]*xsect_27/xsect[6];
								double yy_22_2 = yy_24_2 + sm[2-1][(-1+2)*(2*2+1)+(-1+2)][pk]*xsect_25/xsect[4];
								double yy_21_2 = yy_22_2 + sm[2-1][(-1+2)*(2*2+1)+(-2+2)][pk]*xsect_24/xsect[3];
								double yy_24_1 = yy_21_2 + sm[2-1][(-1+2)*(2*2+1)+(1+2)][pk]*xsect_17/xsect[6];
								double yy_23_1 = yy_24_1 + sm[2-1][(-1+2)*(2*2+1)+(0+2)][pk]*xsect_16/xsect[5];
								double yy_22_1 = yy_23_1 + sm[2-1][(-1+2)*(2*2+1)+(-1+2)][pk]*xsect_15/xsect[4];
								
								if(y<yy_25_3){
									//stays at F=2,M=2;
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<40){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_24_3){
									//transit from F=2,M=2, to F=2,M=1
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<24){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<56){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_23_3){
									//transit from F=2,M=2, to F=2, M=0
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<12){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<48){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_22_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<4){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<36){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_21_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 4; 
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_25_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_24_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<15){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<20){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_22_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_21_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_24_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<1){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<4){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_23_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<3){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<7){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_22_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<6){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<9){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}
							}else{
								p+=1;
							}		
						}
					break;}
					case 5:{
						while(p<kp){
							double time_current_s = ix*time_delta_s; // simulation step time
							if(ix>=steps || time_current_s>time_readin[data_length-1]){
								break; // this is just to avoid small value overflow the interpolation
							}
							double intensity_current_w_m2 = gsl_spline_eval(pulse_data_spline, time_current_s, pulse_data_acc);//intensity at the simulation time
							// define total intensity at mesosphere in watts/m^2
							double intensity = intensity_current_w_m2*intensity_normalized_factor; //10W/m^2, change it to suit applications

							// define conversion rate for power/arcsec <- photon_number/sec
							double magic_number = 8192.33 * time_delta_s * intensity;
							
							// calculate saturation parameters, laster intensity/saturation intensity
							for(int i=0;i<rows*columns;i++){
								if(saturation_intensity[i]!=0){
									saturation_parameter[i] = intensity/saturation_intensity[i];
								}
							}
							
							saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *= alpha;			

							velocity_sodium_pre = velocity_sodium_after;
							double collision_probability = 2*K*temperature*ns*thetas/sqrt(M_PI)/mass_nitrogen/velocity_sodium_pre*psi(velocity_sodium_pre*sqrt(mass_nitrogen/(2*K*temperature)))*time_delta_s;
							bool collided = (collision_probability>gsl_rng_uniform_pos(random_generator));	
							double collision_doppler_mhz = 0;
							if(collided && flag_spin_exchange == 1){
								particle_collision_count += 1;
								particle_collision_count  = particle_collision_count % 10;

								// velocity of nitrogen atom, angle angel_sodium_nitrogen is between Na atom velocity and N2 atom velocity
								double velocity_nitrogen = 415*pow((-log(gsl_rng_uniform_pos(random_generator))),0.4);
								double angel_sodium_nitrogen = acos(2*(gsl_rng_uniform_pos(random_generator)-0.5));
								// relative velocity between two particles
								double velocity_relative_sodium_nitrogen = sqrt(pow(velocity_sodium_pre,2)+pow(velocity_nitrogen,2)-2*velocity_sodium_pre*velocity_nitrogen*cos(angel_sodium_nitrogen));
								double angel_relative_sodium_nitrogen = pow(velocity_nitrogen,2.)>=(pow(velocity_sodium_pre,2.)+pow(velocity_relative_sodium_nitrogen,2.))? M_PI-asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen):asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen);//calculate relative velocity direction respect to sodium atom
								
								// hard sphere interaction in CM reference frame
								double xx_hs = pow(gsl_rng_uniform_pos(random_generator),0.5);
								double thetacm_hs = 2*acos(xx_hs);
								double dvnacm_hs = (relative_mass/(1+relative_mass))*velocity_relative_sodium_nitrogen;
								double dvna_hs = 2*dvnacm_hs*sin(thetacm_hs/2);
								double theta_hs = atan(sin(thetacm_hs)/(1-cos(thetacm_hs)));
								double psi_hs = 2*M_PI*gsl_rng_uniform_pos(random_generator);
								double dvna1_hs = dvna_hs*pow(pow(cos(theta_hs),2)+pow(sin(theta_hs)*cos(psi_hs),2),0.5);
								double theta1_hs = atan(sin(theta_hs)*cos(psi_hs)/cos(theta_hs));
								double phi_hs = angel_relative_sodium_nitrogen + theta1_hs;
								
								velocity_sodium_after = sqrt((pow(velocity_sodium_pre,2.0)+pow(dvna1_hs,2.0)-2*velocity_sodium_pre*dvna1_hs*cos(phi_hs))+pow(dvna_hs*sin(theta_hs)*sin(psi_hs),2.0));
								double beta1 = 2*M_PI*gsl_rng_uniform_pos(random_generator)+atan(dvna_hs*sin(theta_hs)*sin(psi_hs)/dvna1_hs);
								double dalfa =  acos((pow(velocity_sodium_after,2.)+pow(velocity_sodium_pre,2.0)-pow(dvna_hs,2.))/(2*velocity_sodium_after*velocity_sodium_pre));
								angle_sodium_laser = acos(cos(angle_sodium_laser)*cos(dalfa)+sin(angle_sodium_laser)*sin(dalfa)*cos(M_PI-beta1));
								collision_doppler_mhz = velocity_sodium_after*cos(angle_sodium_laser)*nu_na/(2.998*1E8)/1e6;
								if(particle_collision_count==0&&flag_oxygen_spin_exchange){
									// this is going to hit with an oxygen particle.
									// 1 in 5 in air is oxygen, spin exchange by 1/2, so 1 in 10s collision will be for oxygen
									double trial_possibility = gsl_rng_uniform_pos(random_generator);	
									if(flag_current_m_state==0){
										if(trial_possibility<=1/16){flag_current_m_state=1;break;}
										if(trial_possibility<=1/8 && trial_possibility>1/16){flag_current_m_state=5;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=3;break;}
										// or it may just stay in this state
									}else if(flag_current_m_state==1){
										if(trial_possibility<=1/16){flag_current_m_state=2;break;}
										if(trial_possibility<=1/4 && trial_possibility>1/16){flag_current_m_state=6;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/4){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=4;break;}
										if(trial_possibility<=3/4 && trial_possibility>11/16){flag_current_m_state = 0;break;}
									}else if(flag_current_m_state==2){
										if(trial_possibility<=3/8){flag_current_m_state=7;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=6;break;}
										if(trial_possibility<=10/16 && trial_possibility>9/16){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=1;break;}
									}else if(flag_current_m_state==3){
										if(trial_possibility<=1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/8){flag_current_m_state=0;break;}
									}else if(flag_current_m_state==4){
										if(trial_possibility<=3/16){flag_current_m_state=5;break;}
										if(trial_possibility<=3/8 && trial_possibility>3/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=0;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=3;break;}
									}else if(flag_current_m_state==5){
										if(trial_possibility<=3/16){flag_current_m_state=6;break;}
										if(trial_possibility<=4/16 && trial_possibility>3/16){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>4/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>8/16){flag_current_m_state=0;break;}
										if(trial_possibility<=12/16 && trial_possibility>9/16){
										flag_current_m_state=4;break;}
									}else if(flag_current_m_state==6){
										if(trial_possibility<=1/8){flag_current_m_state=7;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>5/16){flag_current_m_state=1;break;}
										if(trial_possibility<=11/16 && trial_possibility>8/16){flag_current_m_state=5;break;}
									}else if(flag_current_m_state==7){
										if(trial_possibility<=1/8){flag_current_m_state=6;break;}
										if(trial_possibility<=4/8 && trial_possibility>1/8){flag_current_m_state=2;break;}
									}
								}								
							}
							
							int freq_idx[6];
							double intensity_partial[6];
							for(int i=0;i<6;i++){
								freq_idx[i] = lround((nu-nu0+collision_doppler_mhz+freq_offset_mhz[i]-freq_nu_start_mhz)*step_per_mhz);
								if((freq_idx[i]<0)||(freq_idx[i]>=freq_nu_length)){
									intensity_partial[i] = 0;
								}else{
									intensity_partial[i] = int_laser_atom[freq_idx[i]];
								}
							}
							
							double xsect[8];
							// start index for F=1,M=1 lower state, relating to the intensity_partial definition 
							int xsect_idx_l_1_start = 2; 
							// start index for F=2,M=2 lower state, relating to the intensity_partial definition
							int xsect_idx_l_2_start = 5; 
							
							// considering saturation intensity, this step calculate the cross section that the laser will hit a sodium atom
							// ground level F=1, M=1, transition_matrix is defined as upper state -> lower state, 
							// calculation is done by Edward, "ESO study", eq 26.
							//transition to lower F=1,M=1 from F'=2, M=2 , upper level 2 to ground state 3
							double xsect_13 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)]));
							//transition from lower F=1,M=1 to F'=2, M=1
							double xsect_23 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)]));
							xsect[2] = xsect_13 + xsect_23;															
							//transition from lower F=1,M=0 to F'=0, M=0							
							double xsect_02 = transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-2]/(1+intensity_partial[xsect_idx_l_1_start-2]*saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)]));
							//transition from lower F=1,M=0 to F'=2, M=0
							double xsect_22 = transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)]));
							xsect[1] = xsect_02 + xsect_22;															
							//transition from lower F=1,M=-1 to F'=1, M=-1							
							double xsect_11 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)]));
							//transition from lower F=1,M=-1 to F'=2, M=-1
							double xsect_21 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)]));
							xsect[0] = xsect_11 + xsect_21;	
														
							//ground level F=2, M=2
							//transition from lower F=2,M=2 to F'=2, M=2							
							double xsect_28 = transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)]));
							//transition from lower F=2,M=2 to F'=3, M=2
							double xsect_38 = transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)]));
							xsect[7] = xsect_28 + xsect_38;															
							
							//ground level F=2, M=1
							//transition from lower F=2,M=1 to F'=1, M=1							
							double xsect_17 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=2, M=1							
							double xsect_27 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=3, M=1
							double xsect_37 = transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)]));
							xsect[6] = xsect_17 + xsect_27 + xsect_37;															
							
							//ground level F=2, M=0
							//transition from lower F=2,M=0 to F'=1, M=0							
							double xsect_16 = transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)]));
							//transition from lower F=2,M=0 to F'=3, M=0
							double xsect_36 = transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)]));
							xsect[5] = xsect_16 + xsect_36;															
							
							//ground level F=2, M=-1
							//transition from lower F=2,M=-1 to F'=1, M=-1							
							double xsect_15 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=2, M=-1							
							double xsect_25 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=3, M=-1
							double xsect_35 = transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)]));
							xsect[4] = xsect_15 + xsect_25 + xsect_35;															

							//ground level F=2, M=-2
							//transition from lower F=2,M=-2 to F'=2, M=-2							
							double xsect_24 = transition_matrix[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)]));
							//transition from lower F=2,M=-2 to F'=3, M=-2
							double xsect_34 = transition_matrix[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)]));
							xsect[3] = xsect_24 + xsect_34;	
							
							// total time increment
							ix+=1; 
							// n is floor(larmor_precession_time/delta_time)
							pk = p%n; 
							if(flag_magnetic_field==0){
								// when it does not need to consider about magnetic level change 
								pk = 0; 
							}
							// sm indexing is defined as sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j]
							double state_xsect = (xsect[idx_l_2_center-2]*sm[2-1][(0+2)*(2*2+1)+(2-2)][pk]) 
									+(xsect[idx_l_2_center-1]*sm[2-1][(0+2)*(2*2+1)+(2-1)][pk])
									+(xsect[idx_l_2_center+0]*sm[2-1][(0+2)*(2*2+1)+(2+0)][pk])
									+(xsect[idx_l_2_center+1]*sm[2-1][(0+2)*(2*2+1)+(2+1)][pk])
									+(xsect[idx_l_2_center+2]*sm[2-1][(0+2)*(2*2+1)+(2+2)][pk]);
							double prob_xsect = state_xsect * magic_number;
							double prob_current = gsl_rng_uniform_pos(random_generator);
							
							if(prob_current<prob_xsect){
								// radiation pressure caused red shift
								nu += 0.05*flag_radiation_pressure; 
								double y = gsl_rng_uniform_pos(random_generator);
								// select which upper level gets chosen
								double yy_35_3 =           sm[2-1][(0+2)*(2*2+1)+(2+2)][pk]*xsect_38/xsect[7];
								double yy_34_3 = yy_35_3 + sm[2-1][(0+2)*(2*2+1)+(1+2)][pk]*xsect_37/xsect[6];
								double yy_33_3 = yy_34_3 + sm[2-1][(0+2)*(2*2+1)+(0+2)][pk]*xsect_36/xsect[5];
								double yy_32_3 = yy_33_3 + sm[2-1][(0+2)*(2*2+1)+(-1+2)][pk]*xsect_35/xsect[4];
								double yy_31_3 = yy_32_3 + sm[2-1][(0+2)*(2*2+1)+(-2+2)][pk]*xsect_34/xsect[3];
								double yy_35_2 = yy_31_3 + sm[2-1][(0+2)*(2*2+1)+(2+2)][pk]*xsect_28/xsect[7];
								double yy_34_2 = yy_35_2 + sm[2-1][(0+2)*(2*2+1)+(1+2)][pk]*xsect_27/xsect[6];
								double yy_32_2 = yy_34_2 + sm[2-1][(0+2)*(2*2+1)+(-1+2)][pk]*xsect_25/xsect[4];
								double yy_31_2 = yy_32_2 + sm[2-1][(0+2)*(2*2+1)+(-2+2)][pk]*xsect_24/xsect[3];
								double yy_34_1 = yy_31_2 + sm[2-1][(0+2)*(2*2+1)+(1+2)][pk]*xsect_17/xsect[6];
								double yy_33_1 = yy_34_1 + sm[2-1][(0+2)*(2*2+1)+(0-+2)][pk]*xsect_16/xsect[5];
								double yy_32_1 = yy_33_1 + sm[2-1][(0+2)*(2*2+1)+(-1+2)][pk]*xsect_15/xsect[4];
								
								if(y<yy_35_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<40){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_34_3){
									//transit from F=2,M=2, to F=2,M=1
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<24){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<56){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_33_3){
									//transit from F=2,M=2, to F=2, M=0
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<12){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<48){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_32_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<4){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<36){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_31_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 4; 
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_35_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_34_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<15){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<20){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_32_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_31_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_34_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<1){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<4){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_33_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<3){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<7){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_32_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<6){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<9){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}
							}else{
								p += 1;
							}					
						}
					break;}
					case 6:{
						// F=2 M=1
						while(p<kp){
							// simulation step time
							double time_current_s = ix*time_delta_s;
							if(ix>=steps || time_current_s>time_readin[data_length-1]){
								break; 
							}
							// intensity at the simulation time
							double intensity_current_w_m2 = gsl_spline_eval(pulse_data_spline, time_current_s, pulse_data_acc);
							// define total intensity at mesosphere in watts/m^2
							double intensity = intensity_current_w_m2*intensity_normalized_factor; //10W/m^2, change it to suit applications

							// define conversion rate for power/arcsec <- photon_number/sec
							double magic_number = 8192.33 * time_delta_s * intensity;
							
							// calculate saturation parameters, laster intensity/saturation intensity
							for(int i=0;i<rows*columns;i++){
								if(saturation_intensity[i]!=0){
									saturation_parameter[i] = intensity/saturation_intensity[i];
								}
							}

							saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *= alpha;
								
							velocity_sodium_pre = velocity_sodium_after;
							double collision_probability = 2*K*temperature*ns*thetas/sqrt(M_PI)/mass_nitrogen/velocity_sodium_pre*psi(velocity_sodium_pre*sqrt(mass_nitrogen/(2*K*temperature)))*time_delta_s;
							bool collided = (collision_probability>gsl_rng_uniform_pos(random_generator));	
							double collision_doppler_mhz = 0;
							if(collided && flag_spin_exchange == 1){
								particle_collision_count += 1;
								particle_collision_count  = particle_collision_count % 10;

								// velocity of nitrogen atom, angle angel_sodium_nitrogen is between Na atom velocity and N2 atom velocity
								double velocity_nitrogen = 415*pow((-log(gsl_rng_uniform_pos(random_generator))),0.4);
								double angel_sodium_nitrogen = acos(2*(gsl_rng_uniform_pos(random_generator)-0.5));
								// relative velocity between two particles
								double velocity_relative_sodium_nitrogen = sqrt(pow(velocity_sodium_pre,2)+pow(velocity_nitrogen,2)-2*velocity_sodium_pre*velocity_nitrogen*cos(angel_sodium_nitrogen));
								double angel_relative_sodium_nitrogen = pow(velocity_nitrogen,2.)>=(pow(velocity_sodium_pre,2.)+pow(velocity_relative_sodium_nitrogen,2.))? M_PI-asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen):asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen);//calculate relative velocity direction respect to sodium atom
								
								// hard sphere interaction in CM reference frame
								double xx_hs = pow(gsl_rng_uniform_pos(random_generator),0.5);
								double thetacm_hs = 2*acos(xx_hs);
								double dvnacm_hs = (relative_mass/(1+relative_mass))*velocity_relative_sodium_nitrogen;
								double dvna_hs = 2*dvnacm_hs*sin(thetacm_hs/2);
								double theta_hs = atan(sin(thetacm_hs)/(1-cos(thetacm_hs)));
								double psi_hs = 2*M_PI*gsl_rng_uniform_pos(random_generator);
								double dvna1_hs = dvna_hs*pow(pow(cos(theta_hs),2)+pow(sin(theta_hs)*cos(psi_hs),2),0.5);
								double theta1_hs = atan(sin(theta_hs)*cos(psi_hs)/cos(theta_hs));
								double phi_hs = angel_relative_sodium_nitrogen + theta1_hs;
								
								velocity_sodium_after = sqrt((pow(velocity_sodium_pre,2.0)+pow(dvna1_hs,2.0)-2*velocity_sodium_pre*dvna1_hs*cos(phi_hs))+pow(dvna_hs*sin(theta_hs)*sin(psi_hs),2.0));
								double beta1 = 2*M_PI*gsl_rng_uniform_pos(random_generator)+atan(dvna_hs*sin(theta_hs)*sin(psi_hs)/dvna1_hs);
								double dalfa =  acos((pow(velocity_sodium_after,2.)+pow(velocity_sodium_pre,2.0)-pow(dvna_hs,2.))/(2*velocity_sodium_after*velocity_sodium_pre));
								angle_sodium_laser = acos(cos(angle_sodium_laser)*cos(dalfa)+sin(angle_sodium_laser)*sin(dalfa)*cos(M_PI-beta1));
								collision_doppler_mhz = velocity_sodium_after*cos(angle_sodium_laser)*nu_na/(2.998*1E8)/1e6;
								if(particle_collision_count==0&&flag_oxygen_spin_exchange){
									// this is going to hit with an oxygen particle.
									// 1 in 5 in air is oxygen, spin exchange by 1/2, so 1 in 10s collision will be for oxygen
									double trial_possibility = gsl_rng_uniform_pos(random_generator);	
									if(flag_current_m_state==0){
										if(trial_possibility<=1/16){flag_current_m_state=1;break;}
										if(trial_possibility<=1/8 && trial_possibility>1/16){flag_current_m_state=5;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=3;break;}
										// or it may just stay in this state
									}else if(flag_current_m_state==1){
										if(trial_possibility<=1/16){flag_current_m_state=2;break;}
										if(trial_possibility<=1/4 && trial_possibility>1/16){flag_current_m_state=6;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/4){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=4;break;}
										if(trial_possibility<=3/4 && trial_possibility>11/16){flag_current_m_state = 0;break;}
									}else if(flag_current_m_state==2){
										if(trial_possibility<=3/8){flag_current_m_state=7;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=6;break;}
										if(trial_possibility<=10/16 && trial_possibility>9/16){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=1;break;}
									}else if(flag_current_m_state==3){
										if(trial_possibility<=1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/8){flag_current_m_state=0;break;}
									}else if(flag_current_m_state==4){
										if(trial_possibility<=3/16){flag_current_m_state=5;break;}
										if(trial_possibility<=3/8 && trial_possibility>3/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=0;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=3;break;}
									}else if(flag_current_m_state==5){
										if(trial_possibility<=3/16){flag_current_m_state=6;break;}
										if(trial_possibility<=4/16 && trial_possibility>3/16){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>4/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>8/16){flag_current_m_state=0;break;}
										if(trial_possibility<=12/16 && trial_possibility>9/16){
										flag_current_m_state=4;break;}
									}else if(flag_current_m_state==6){
										if(trial_possibility<=1/8){flag_current_m_state=7;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>5/16){flag_current_m_state=1;break;}
										if(trial_possibility<=11/16 && trial_possibility>8/16){flag_current_m_state=5;break;}
									}else if(flag_current_m_state==7){
										if(trial_possibility<=1/8){flag_current_m_state=6;break;}
										if(trial_possibility<=4/8 && trial_possibility>1/8){flag_current_m_state=2;break;}
									}
								}								
							}
							
							int freq_idx[6];
							double intensity_partial[6];
							for(int i=0;i<6;i++){
								freq_idx[i] = lround((nu-nu0+collision_doppler_mhz+freq_offset_mhz[i]-freq_nu_start_mhz)*step_per_mhz);
								if((freq_idx[i]<0)||(freq_idx[i]>=freq_nu_length)){
									intensity_partial[i] = 0;
								}else{
									intensity_partial[i] = int_laser_atom[freq_idx[i]];
								}
							}
							
							double xsect[8];
							// start index for F=1,M=1 lower state, relating to the intensity_partial definition 
							int xsect_idx_l_1_start = 2; 
							// start index for F=2,M=2 lower state, relating to the intensity_partial definition
							int xsect_idx_l_2_start = 5; 
							// considering saturation intensity, this step calculate the cross section that the laser will hit a sodium atom
							// ground level F=1, M=1, transition_matrix is defined as upper state -> lower state, 
							// calculation is done by Edward, "ESO study", eq 26.
							//transition to lower F=1,M=1 from F'=2, M=2 , upper level 2 to ground state 3
							double xsect_13 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)]));
							//transition from lower F=1,M=1 to F'=2, M=1
							double xsect_23 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)]));
							xsect[2] = xsect_13 + xsect_23;															
							//transition from lower F=1,M=0 to F'=0, M=0							
							double xsect_02 = transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-2]/(1+intensity_partial[xsect_idx_l_1_start-2]*saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)]));
							//transition from lower F=1,M=0 to F'=2, M=0
							double xsect_22 = transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)]));
							xsect[1] = xsect_02 + xsect_22;															
							//transition from lower F=1,M=-1 to F'=1, M=-1							
							double xsect_11 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)]));
							//transition from lower F=1,M=-1 to F'=2, M=-1
							double xsect_21 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)]));
							xsect[0] = xsect_11 + xsect_21;	
														
							//ground level F=2, M=2
							//transition from lower F=2,M=2 to F'=2, M=2							
							double xsect_28 = transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)]));
							//transition from lower F=2,M=2 to F'=3, M=2
							double xsect_38 = transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)]));
							xsect[7] = xsect_28 + xsect_38;															
							
							//ground level F=2, M=1
							//transition from lower F=2,M=1 to F'=1, M=1							
							double xsect_17 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=2, M=1							
							double xsect_27 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)]));
							//transition from lower F=2,M=1 to F'=3, M=1
							double xsect_37 = transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)]));
							xsect[6] = xsect_17 + xsect_27 + xsect_37;															
							
							//ground level F=2, M=0
							//transition from lower F=2,M=0 to F'=1, M=0							
							double xsect_16 = transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)]));
							//transition from lower F=2,M=0 to F'=3, M=0
							double xsect_36 = transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)]));
							xsect[5] = xsect_16 + xsect_36;															
							
							//ground level F=2, M=-1
							//transition from lower F=2,M=-1 to F'=1, M=-1							
							double xsect_15 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=2, M=-1							
							double xsect_25 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)]));
							//transition from lower F=2,M=-1 to F'=3, M=-1
							double xsect_35 = transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)]));
							xsect[4] = xsect_15 + xsect_25 + xsect_35;															

							//ground level F=2, M=-2
							//transition from lower F=2,M=-2 to F'=2, M=-2							
							double xsect_24 = transition_matrix[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)]));
							//transition from lower F=2,M=-2 to F'=3, M=-2
							double xsect_34 = transition_matrix[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)]));
							xsect[3] = xsect_24 + xsect_34;	
												
							// total time increment
							ix+=1; 
							// n is floor(larmor_precession_time/delta_time)
							pk = p%n; 
							if(flag_magnetic_field==0){
								pk = 0; 
							}
							// sm indexing is defined as sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j]
							double state_xsect = (xsect[idx_l_2_center-2]*sm[2-1][(2+1)*(2*2+1)+(2-2)][pk]) 
									+(xsect[idx_l_2_center-1]*sm[2-1][(2+1)*(2*2+1)+(2-1)][pk])
									+(xsect[idx_l_2_center+0]*sm[2-1][(2+1)*(2*2+1)+(2+0)][pk])
									+(xsect[idx_l_2_center+1]*sm[2-1][(2+1)*(2*2+1)+(2+1)][pk])
									+(xsect[idx_l_2_center+2]*sm[2-1][(2+1)*(2*2+1)+(2+2)][pk]);
							double prob_xsect = state_xsect * magic_number;
							double prob_current = gsl_rng_uniform_pos(random_generator);
							
							if(prob_current<prob_xsect){
								// radiation pressure caused red shift
								nu += 0.05*flag_radiation_pressure; 
								double y = gsl_rng_uniform_pos(random_generator);
								// select which upper level gets chosen
								double yy_45_3 =           sm[2-1][(1+2)*(2*2+1)+(2+2)][pk]*xsect_38/xsect[7];
								double yy_44_3 = yy_45_3 + sm[2-1][(1+2)*(2*2+1)+(1+2)][pk]*xsect_37/xsect[6];
								double yy_43_3 = yy_44_3 + sm[2-1][(1+2)*(2*2+1)+(0+2)][pk]*xsect_36/xsect[5];
								double yy_42_3 = yy_43_3 + sm[2-1][(1+2)*(2*2+1)+(-1+2)][pk]*xsect_35/xsect[4];
								double yy_41_3 = yy_42_3 + sm[2-1][(1+2)*(2*2+1)+(-2+2)][pk]*xsect_34/xsect[3];
								double yy_45_2 = yy_41_3 + sm[2-1][(1+2)*(2*2+1)+(2+2)][pk]*xsect_28/xsect[7];
								double yy_44_2 = yy_45_2 + sm[2-1][(1+2)*(2*2+1)+(1+2)][pk]*xsect_27/xsect[6];
								double yy_42_2 = yy_44_2 + sm[2-1][(1+2)*(2*2+1)+(-1+2)][pk]*xsect_25/xsect[4];
								double yy_41_2 = yy_42_2 + sm[2-1][(1+2)*(2*2+1)+(-2+2)][pk]*xsect_24/xsect[3];
								double yy_44_1 = yy_41_2 + sm[2-1][(1+2)*(2*2+1)+(1+2)][pk]*xsect_17/xsect[6];
								double yy_43_1 = yy_44_1 + sm[2-1][(1+2)*(2*2+1)+(0-+2)][pk]*xsect_16/xsect[5];
								double yy_42_1 = yy_43_1 + sm[2-1][(1+2)*(2*2+1)+(-1+2)][pk]*xsect_15/xsect[4];
								
								if(y<yy_45_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<40){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_44_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<24){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<56){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_43_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<12){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<48){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_42_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<4){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<36){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_41_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 4; 
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_45_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_44_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<15){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<20){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_42_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_41_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_44_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<1){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<4){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_43_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<3){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<7){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_42_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<6){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<9){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}
							}else{
								p += 1;
							}
						}
					break;}
					case 7:{
					// F=2 M=2
						while(p<kp){
							// simulation step time
							double time_current_s = ix*time_delta_s; 
							if(ix>=steps || time_current_s>time_readin[data_length-1]){
								break;
							}
							// intensity at the simulation time
							double intensity_current_w_m2 = gsl_spline_eval(pulse_data_spline, time_current_s, pulse_data_acc);
							// define total intensity at mesosphere in watts/m^2
							double intensity = intensity_current_w_m2*intensity_normalized_factor; 
							// define conversion rate for power/arcsec <- photon_number/sec
							double magic_number = 8192.33 * time_delta_s * intensity;
							// calculate saturation parameters, laster intensity/saturation intensity
							for(int i=0;i<rows*columns;i++){
								if(saturation_intensity[i]!=0){
									saturation_parameter[i] = intensity/saturation_intensity[i];
								}
							}
							
							saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *= alpha;
							saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *= alpha;

							velocity_sodium_pre = velocity_sodium_after;
							double collision_probability = 2*K*temperature*ns*thetas/sqrt(M_PI)/mass_nitrogen/velocity_sodium_pre*psi(velocity_sodium_pre*sqrt(mass_nitrogen/(2*K*temperature)))*time_delta_s;
							bool collided = (collision_probability>gsl_rng_uniform_pos(random_generator));	
							double collision_doppler_mhz = 0;
							if(collided && flag_spin_exchange == 1){
								particle_collision_count += 1;
								particle_collision_count  = particle_collision_count % 10;
								// velocity of nitrogen atom, angle angel_sodium_nitrogen is between Na atom velocity and N2 atom velocity
								double velocity_nitrogen = 415*pow((-log(gsl_rng_uniform_pos(random_generator))),0.4);
								double angel_sodium_nitrogen = acos(2*(gsl_rng_uniform_pos(random_generator)-0.5));
								// relative velocity between two particles
								double velocity_relative_sodium_nitrogen = sqrt(pow(velocity_sodium_pre,2)+pow(velocity_nitrogen,2)-2*velocity_sodium_pre*velocity_nitrogen*cos(angel_sodium_nitrogen));
								// calculate relative velocity direction respect to sodium atom
								double angel_relative_sodium_nitrogen = pow(velocity_nitrogen,2.)>=(pow(velocity_sodium_pre,2.)+pow(velocity_relative_sodium_nitrogen,2.))? M_PI-asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen):asin(sin(angel_sodium_nitrogen)*velocity_nitrogen/velocity_relative_sodium_nitrogen);
								// hard sphere interaction in CM reference frame
								double xx_hs = pow(gsl_rng_uniform_pos(random_generator),0.5);
								double thetacm_hs = 2*acos(xx_hs);
								double dvnacm_hs = (relative_mass/(1+relative_mass))*velocity_relative_sodium_nitrogen;
								double dvna_hs = 2*dvnacm_hs*sin(thetacm_hs/2);
								double theta_hs = atan(sin(thetacm_hs)/(1-cos(thetacm_hs)));
								double psi_hs = 2*M_PI*gsl_rng_uniform_pos(random_generator);
								double dvna1_hs = dvna_hs*pow(pow(cos(theta_hs),2)+pow(sin(theta_hs)*cos(psi_hs),2),0.5);
								double theta1_hs = atan(sin(theta_hs)*cos(psi_hs)/cos(theta_hs));
								double phi_hs = angel_relative_sodium_nitrogen + theta1_hs;
								velocity_sodium_after = sqrt((pow(velocity_sodium_pre,2.0)+pow(dvna1_hs,2.0)-2*velocity_sodium_pre*dvna1_hs*cos(phi_hs))+pow(dvna_hs*sin(theta_hs)*sin(psi_hs),2.0));
								double beta1 = 2*M_PI*gsl_rng_uniform_pos(random_generator)+atan(dvna_hs*sin(theta_hs)*sin(psi_hs)/dvna1_hs);
								double dalfa =  acos((pow(velocity_sodium_after,2.)+pow(velocity_sodium_pre,2.0)-pow(dvna_hs,2.))/(2*velocity_sodium_after*velocity_sodium_pre));
								angle_sodium_laser = acos(cos(angle_sodium_laser)*cos(dalfa)+sin(angle_sodium_laser)*sin(dalfa)*cos(M_PI-beta1));
								collision_doppler_mhz = velocity_sodium_after*cos(angle_sodium_laser)*nu_na/(2.998*1E8)/1e6;
								if(particle_collision_count==0&&flag_oxygen_spin_exchange){
									// this is going to hit with an oxygen particle.
									// 1 in 5 in air is oxygen, spin exchange by 1/2, so 1 in 10s collision will be for oxygen
									double trial_possibility = gsl_rng_uniform_pos(random_generator);	
									if(flag_current_m_state==0){
										if(trial_possibility<=1/16){flag_current_m_state=1;break;}
										if(trial_possibility<=1/8 && trial_possibility>1/16){flag_current_m_state=5;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=3;break;}
										// or it may just stay in this state
									}else if(flag_current_m_state==1){
										if(trial_possibility<=1/16){flag_current_m_state=2;break;}
										if(trial_possibility<=1/4 && trial_possibility>1/16){flag_current_m_state=6;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/4){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/16){flag_current_m_state=4;break;}
										if(trial_possibility<=3/4 && trial_possibility>11/16){flag_current_m_state = 0;break;}
									}else if(flag_current_m_state==2){
										if(trial_possibility<=3/8){flag_current_m_state=7;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=6;break;}
										if(trial_possibility<=10/16 && trial_possibility>9/16){flag_current_m_state=5;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=1;break;}
									}else if(flag_current_m_state==3){
										if(trial_possibility<=1/8){flag_current_m_state=4;break;}
										if(trial_possibility<=1/2 && trial_possibility>1/8){flag_current_m_state=0;break;}
									}else if(flag_current_m_state==4){
										if(trial_possibility<=3/16){flag_current_m_state=5;break;}
										if(trial_possibility<=3/8 && trial_possibility>3/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>3/8){flag_current_m_state=0;break;}
										if(trial_possibility<=11/16 && trial_possibility>5/8){flag_current_m_state=3;break;}
									}else if(flag_current_m_state==5){
										if(trial_possibility<=3/16){flag_current_m_state=6;break;}
										if(trial_possibility<=4/16 && trial_possibility>3/16){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>4/16){flag_current_m_state=1;break;}
										if(trial_possibility<=9/16 && trial_possibility>8/16){flag_current_m_state=0;break;}
										if(trial_possibility<=12/16 && trial_possibility>9/16){
										flag_current_m_state=4;break;}
									}else if(flag_current_m_state==6){
										if(trial_possibility<=1/8){flag_current_m_state=7;break;}
										if(trial_possibility<=5/16 && trial_possibility>1/8){flag_current_m_state=2;break;}
										if(trial_possibility<=8/16 && trial_possibility>5/16){flag_current_m_state=1;break;}
										if(trial_possibility<=11/16 && trial_possibility>8/16){flag_current_m_state=5;break;}
									}else if(flag_current_m_state==7){
										if(trial_possibility<=1/8){flag_current_m_state=6;break;}
										if(trial_possibility<=4/8 && trial_possibility>1/8){flag_current_m_state=2;break;}
									}
								}								
							}
															
							int freq_idx[6];
							double intensity_partial[6];
							for(int i=0;i<6;i++){
								freq_idx[i] = lround((nu-nu0+collision_doppler_mhz+freq_offset_mhz[i]-freq_nu_start_mhz)*step_per_mhz);
								if((freq_idx[i]<0)||(freq_idx[i]>=freq_nu_length)){
									intensity_partial[i] = 0;
								}else{
									intensity_partial[i] = int_laser_atom[freq_idx[i]];
								}
							}
							
							double xsect[8];
							// start index for F=1,M=1 lower state, relating to the intensity_partial definition
							int xsect_idx_l_1_start = 2; 
							// start index for F=2,M=2 lower state, relating to the intensity_partial definition							
							int xsect_idx_l_2_start = 5; 
							
							// considering saturation intensity, this step calculate the cross section that the laser will hit a sodium atom
							// ground level F=1, M=1, transition_matrix is defined as upper state -> lower state, 
							// calculation is done by Edward, "ESO study", eq 26.
							// transition to lower F=1,M=1 from F'=2, M=2 , upper level 2 to ground state 3
							double xsect_13 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_1_center+1,columns)]));
							// transition from lower F=1,M=1 to F'=2, M=1
							double xsect_23 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_1_center+1,columns)]));
							xsect[2] = xsect_13 + xsect_23;															
							// transition from lower F=1,M=0 to F'=0, M=0							
							double xsect_02 = transition_matrix[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-2]/(1+intensity_partial[xsect_idx_l_1_start-2]*saturation_parameter[atmatrix_r(idx_u_f_0_center+0,idx_l_f_1_center+0,columns)]));
							// transition from lower F=1,M=0 to F'=2, M=0
							double xsect_22 = transition_matrix[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center+0,idx_l_f_1_center+0,columns)]));
							xsect[1] = xsect_02 + xsect_22;															
							// transition from lower F=1,M=-1 to F'=1, M=-1							
							double xsect_11 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start-1]/(1+intensity_partial[xsect_idx_l_1_start-1]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_1_center-1,columns)]));
							// transition from lower F=1,M=-1 to F'=2, M=-1
							double xsect_21 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_1_start]/(1+intensity_partial[xsect_idx_l_1_start]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_1_center-1,columns)]));
							xsect[0] = xsect_11 + xsect_21;	
														
							// ground level F=2, M=2
							// transition from lower F=2,M=2 to F'=2, M=2							
							double xsect_28 = transition_matrix[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+2,idx_l_f_2_center+2,columns)]));
							// transition from lower F=2,M=2 to F'=3, M=2
							double xsect_38 = transition_matrix[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+2,idx_l_f_2_center+2,columns)]));
							xsect[7] = xsect_28 + xsect_38;															
							
							// ground level F=2, M=1
							// transition from lower F=2,M=1 to F'=1, M=1							
							double xsect_17 = transition_matrix[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+1,idx_l_f_2_center+1,columns)]));
							// transition from lower F=2,M=1 to F'=2, M=1							
							double xsect_27 = transition_matrix[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center+1,idx_l_f_2_center+1,columns)]));
							// transition from lower F=2,M=1 to F'=3, M=1
							double xsect_37 = transition_matrix[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+1,idx_l_f_2_center+1,columns)]));
							xsect[6] = xsect_17 + xsect_27 + xsect_37;															
							
							// ground level F=2, M=0
							// transition from lower F=2,M=0 to F'=1, M=0							
							double xsect_16 = transition_matrix[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center+0,idx_l_f_2_center+0,columns)]));
							// transition from lower F=2,M=0 to F'=3, M=0
							double xsect_36 = transition_matrix[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center+0,idx_l_f_2_center+0,columns)]));
							xsect[5] = xsect_16 + xsect_36;															
							
							// ground level F=2, M=-1
							// transition from lower F=2,M=-1 to F'=1, M=-1							
							double xsect_15 = transition_matrix[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-2]/(1+intensity_partial[xsect_idx_l_2_start-2]*saturation_parameter[atmatrix_r(idx_u_f_1_center-1,idx_l_f_2_center-1,columns)]));
							// transition from lower F=2,M=-1 to F'=2, M=-1							
							double xsect_25 = transition_matrix[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-1,idx_l_f_2_center-1,columns)]));
							// transition from lower F=2,M=-1 to F'=3, M=-1
							double xsect_35 = transition_matrix[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-1,idx_l_f_2_center-1,columns)]));
							xsect[4] = xsect_15 + xsect_25 + xsect_35;															

							// ground level F=2, M=-2
							// transition from lower F=2,M=-2 to F'=2, M=-2							
							double xsect_24 = transition_matrix[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start-1]/(1+intensity_partial[xsect_idx_l_2_start-1]*saturation_parameter[atmatrix_r(idx_u_f_2_center-2,idx_l_f_2_center-2,columns)]));
							// transition from lower F=2,M=-2 to F'=3, M=-2
							double xsect_34 = transition_matrix[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)] *
											  (intensity_partial[xsect_idx_l_2_start]/(1+intensity_partial[xsect_idx_l_2_start]*saturation_parameter[atmatrix_r(idx_u_f_3_center-2,idx_l_f_2_center-2,columns)]));
							xsect[3] = xsect_24 + xsect_34;		
							
							// total time increment
							ix+=1; 
							// n is floor(larmor_precession_time/delta_time)
							pk = p%n; 
							if(flag_magnetic_field==0){
								// do not need to consider about magnetic level change 
								pk = 0; 
							}
							// sm indexing is defined as sm[f-1][(m_state+f)*(2*f+1)+(n_state+f)][j]
							double state_xsect = (xsect[idx_l_2_center-2]*sm[2-1][(2+2)*(2*2+1)+(2-2)][pk]) 
									+(xsect[idx_l_2_center-1]*sm[2-1][(2+2)*(2*2+1)+(2-1)][pk])
									+(xsect[idx_l_2_center+0]*sm[2-1][(2+2)*(2*2+1)+(2+0)][pk])
									+(xsect[idx_l_2_center+1]*sm[2-1][(2+2)*(2*2+1)+(2+1)][pk])
									+(xsect[idx_l_2_center+2]*sm[2-1][(2+2)*(2*2+1)+(2+2)][pk]);
							double prob_xsect = state_xsect * magic_number;
							double prob_current = gsl_rng_uniform_pos(random_generator);
							if(prob_current<prob_xsect){
								// radiation pressure caused red shift
								nu += 0.05*flag_radiation_pressure; 
								double y = gsl_rng_uniform_pos(random_generator);
								// select which upper level gets chosen
								double yy_55_3 =           sm[2-1][(2+2)*(2*2+1)+(2+2)][pk]*xsect_38/xsect[7];
								double yy_54_3 = yy_55_3 + sm[2-1][(2+2)*(2*2+1)+(1+2)][pk]*xsect_37/xsect[6];
								double yy_53_3 = yy_54_3 + sm[2-1][(2+2)*(2*2+1)+(0+2)][pk]*xsect_36/xsect[5];
								double yy_52_3 = yy_53_3 + sm[2-1][(2+2)*(2*2+1)+(-1+2)][pk]*xsect_35/xsect[4];
								double yy_51_3 = yy_52_3 + sm[2-1][(2+2)*(2*2+1)+(-2+2)][pk]*xsect_34/xsect[3];
								double yy_55_2 = yy_51_3 + sm[2-1][(2+2)*(2*2+1)+(2+2)][pk]*xsect_28/xsect[7];
								double yy_54_2 = yy_55_2 + sm[2-1][(2+2)*(2*2+1)+(1+2)][pk]*xsect_27/xsect[6];
								double yy_52_2 = yy_54_2 + sm[2-1][(2+2)*(2*2+1)+(-1+2)][pk]*xsect_25/xsect[4];
								double yy_51_2 = yy_52_2 + sm[2-1][(2+2)*(2*2+1)+(-2+2)][pk]*xsect_24/xsect[3];
								double yy_54_1 = yy_51_2 + sm[2-1][(2+2)*(2*2+1)+(1+2)][pk]*xsect_17/xsect[6];
								double yy_53_1 = yy_54_1 + sm[2-1][(2+2)*(2*2+1)+(0-+2)][pk]*xsect_16/xsect[5];
								double yy_52_1 = yy_53_1 + sm[2-1][(2+2)*(2*2+1)+(-1+2)][pk]*xsect_15/xsect[4];
																
								if(y<yy_55_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<40){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_54_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60; // multiplied by 60 to be easily compared with others
									if(yr<24){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<56){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_53_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<12){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<48){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_52_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<4){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<36){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_51_3){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 4; 
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_55_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_54_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<15){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<20){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_52_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<10){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<15){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<45){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_51_2){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<20){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<30){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_54_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<1){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<4){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 7;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}
									break;
								}else if(y<yy_53_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<3){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<7){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 6;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else{
										flag_current_m_state = 2;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}else if(y<yy_52_1){
									double yr = gsl_rng_uniform_pos(random_generator)*60;
									if(yr<6){
										flag_current_m_state = 3;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<9){
										flag_current_m_state = 4;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else if(yr<10){
										flag_current_m_state = 5;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}else if(yr<35){
										flag_current_m_state = 0;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=1.5;
									}else{
										flag_current_m_state = 1;
										nnp[flag_current_m_state]+=1;
										np[flag_current_m_state]+=1;
										r[flag_current_m_state]+=.75;
									}
									break;
								}
							}else{
								p += 1;
							}
						}
					break;}				
					default:{
						fprintf(stderr, "state index is not a valid one\n");
						exit(1);
					}
				}
			}
			
			// for emsemble average final frequency 
			nur += nu;
			
			// counting how many transitions in higher/lower state after one collision lifetime
			if(flag_current_m_state<3){
				nnlower+=1; 
			}else{
				nnupper+=1;
			}
			
			// counting how many photon-returns per steradian
			for(int m=0;m<8;m++){
				rr[ii] += (double)r[m]/4/M_PI;
			}
		}
		
		// if parallel, critical section here
		{
			// bookkeeping works
			double nuff = nur/trials; //average red shift for this velocity group
			if(flag_print_each_step==1)
				fprintf(fid, "\t\tnuff = %f\n", nuff);

			double totalphotons = 0;
			
			nufinal[z] = nuff;
			for(int i=0;i<trials;i++){
				totalphotons += rr[i];
			}
			totalphotons = totalphotons/trials;
			
			upperpop[z] = nnupper/(double)trials;
			lowerpop[z] = nnlower/(double)trials;
			photonreturn[z] = totalphotons;
			
			// factor converting fwhm to theta
			const double fwhm_to_theta = 2.35482;
			// considering the Doppler broadening, upper population
			gausupperpop[z] = upperpop[z]*exp(-1*z*z/(2*pow(freq_fwhm_doppler_mhz/fwhm_to_theta,2)));
			// considering the Doppler broadening, lower population
			gauslowerpop[z] = lowerpop[z]*exp(-1*z*z/(2*pow(freq_fwhm_doppler_mhz/fwhm_to_theta,2))); 
			// considering the Doppler broadening, photon return with this velocity group
			gausphotonreturn[z] =  totalphotons*exp(-1*z*z/(2*pow(freq_fwhm_doppler_mhz/fwhm_to_theta,2)));
			
			if(gausphotonreturn[z]>max_photon){
				max_photon = gausphotonreturn[z];
			}
			if(flag_print_each_step==1){
				fprintf(fid, "\t\tgausupperpop = %f\n", gausupperpop[z]);
				fprintf(fid, "\t\tgauslowerpop = %f\n", gauslowerpop[z]);
				fprintf(fid, "\t\tgausphotonreturn = %E\n", gausphotonreturn[z]);
			}
			
			// this is will be normalized by the gaussian shape's area 
			average_photon += gausphotonreturn[z];
	
			if(z!=freq_nu_start_mhz &&z!=freq_nu_end_mhz){
				velinc[z] = 2*freq_nu_incr_mhz/(abs(nufinal[z-freq_nu_incr_mhz]-nufinal[z+freq_nu_incr_mhz])+1);
			}else{
				velinc[z] = 1;
			}
			
			spectrum[z]= std::pair<double, double>(nufinal[z], totalphotons*exp(-1*z*z/(2*pow(freq_fwhm_doppler_mhz/fwhm_to_theta,2)))*velinc[z]);	
			if(flag_print_each_step==1){
				fprintf(fid, "\t\tz = %f\n", spectrum[z].first);
				fprintf(fid, "\t\tspectrum = %f\n", spectrum[z].second);
			}
		}
	}
	
	// 1065 is the integrate value of the exp(-1*z*z/2*pow(freq_fwhm_doppler_mhz/fwhm_to_theta,2))
	average_photon = average_photon*freq_nu_incr_mhz/1064.4/simulation_time/intensity_w_m2;

	// average photon value;
	if(flag_print_photon_return==1)
		fprintf(fid, "average_photon = %f\n", average_photon);

	// close the output file
	fclose(fid);
		
	// release resources
	gsl_rng_free(random_generator);
	for(int i=0;i<total_f_number;i++){
		for(int j=0;j<(2*(i+1)+1)*(2*(i+1)+1);j++){
			free(sm[i][j]);
		}
		free(sm[i]);
	}	
	free(sm);
	free(saturation_parameter);
	free(transition_matrix);
	free(saturation_intensity);
	free(int_laser);
	free(int_laser_atom);
	config_destroy(&cfg);
	return 0;
}
